<?php
/*------------------------------------*\
		# Include
\*------------------------------------*/
/* Security
-------------------------- */
include get_template_directory() . '/lib/theme-security.php';
include get_template_directory() . '/lib/theme-clean.php';

/* Theme
-------------------------- */
include get_template_directory() . '/lib/theme-support.php';
include get_template_directory() . '/lib/theme-setup.php';
include get_template_directory() . '/lib/theme-nav.php';
include get_template_directory() . '/lib/theme-pagination.php';

/* Assets
-------------------------- */
include get_template_directory() . '/lib/assets-styles.php';
include get_template_directory() . '/lib/assets-scripts.php';



/* CLASS
-------------------------- */
include get_template_directory() . '/Class/Menu.php';

/* Custom
-------------------------- */
include get_template_directory() . '/lib/custom-post-projet.php';
include get_template_directory() . '/lib/custom-post-membre.php';
include get_template_directory() . '/lib/custom-breadcrumb.php';



/*------------------------------------*\
		ShortCode Functions
\*------------------------------------*/

// youtube embed
function youtube($atts, $content = null) {
		return '<div class="embed-video"><iframe src="https://www.youtube.com/embed/'. do_shortcode($content) .'" frameborder="0" allowfullscreen></iframe></div>';
}
add_shortcode('youtube', 'youtube');

// vimeo embed
function vimeo($atts, $content = null) {
		return '<div class="embed-video"><iframe src="https://player.vimeo.com/video/'. do_shortcode($content) .'?title=0&byline=0&portrait=0&badge=0&autoplay=1" frameborder="0" allowfullscreen></iframe></div>';
}
add_shortcode('vimeo', 'vimeo');

/*------------------------------------*\
		SVG
\*------------------------------------*/
// Allow SVG Files
function cc_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');




/*------------------------------------*\
		remove useless rich editor
\*------------------------------------*/


function remove_pages_editor(){
		remove_post_type_support( 'post', 'editor' );
		remove_post_type_support( 'page', 'editor' );
}
add_action( 'init', 'remove_pages_editor' );

function my_format_TinyMCE( $in ) {
	$in['wpautop'] = false;
	return $in;
}
add_filter( 'tiny_mce_before_init', 'my_format_TinyMCE' );
remove_filter('term_description','wpautop');



/*------------------------------------*\
* Callback function to filter the MCE settings
\*------------------------------------*/

add_filter( 'mce_buttons_2', 'sennza_mce_buttons' );
function sennza_mce_buttons( $buttons ) {
  array_unshift( $buttons, 'styleselect' );
  return $buttons;
}


add_filter( 'tiny_mce_before_init', 'enedis_mce_before_init' );
function enedis_mce_before_init( $init_array ) {

  // Add back some more of styles we want to see in TinyMCE
  $init_array['preview_styles'] = "color background-color";

  $style_formats = array(
    // Each array child is a format with it's own settings
    array(
      'title'   => 'Texte en bleu',
      'inline'  => 'span',
      'classes' => 'blue',
    ),
  );

  $init_array['style_formats'] = json_encode( $style_formats );
  return $init_array;
}



/*------------------------------------*\
		Carbon Fields
\*------------------------------------*/
use Carbon_Fields\Container;
use Carbon_Fields\Field;

add_action( 'carbon_fields_register_fields', 'crb_attach_theme_options' );
function crb_attach_theme_options() {
		include_once(dirname(__FILE__) . '/carbon/carbon.php');
}



// add custom style to carbon stuff
add_action('admin_head', 'backend_style');
function backend_style() {
	echo '<style>
		.carbon-container .carbon-separator {
			background: #2f5996;
		}
		#poststuff .carbon-container .carbon-separator h3 {
			color: #ffffff;
			font-size:16px;
			font-family:arial,sans-serif;
		}
		.carbon-container .carbon-groups-holder .carbon-row {
			border-bottom: 2px solid #ddd;
		}
		.carbon-container .carbon-drag-handle {
		}
		body .carbon-container-carbon_fields_container_timeline .carbon-association-list {
			height: 600px;
		}
	</style>';
}

/*------------------------------------*\
		Custom images sizes
\*------------------------------------*/


// JPEG quality > 90
add_filter('jpeg_quality', function($arg){return 90;});


add_theme_support( 'post-thumbnails' );

// TODO: checker pourquoi taille pas prise en compte
// taille d'image à utiliser pour bandeau actu
add_image_size( 'banner', 1240, 370, true );

/* INFOS Tailles images:
- miniatures : thumbnail actu
- banner: image une actu
- moyenne: contenu de page
- grande taille : carousel home et page header
*/


/*------------------------------------*\
		Helpers
\*------------------------------------*/

/**
 * Clean display for mac
 */
// function print_mac($array = array()){
// 	echo '<pre>';
// 	print_r($array);
// 	echo '</pre>';
// }


/*------------------------------------*\
		CUSTOM POSTS
\*------------------------------------*/

/* Sort by Taxonomy
*******************/
/*function pippin_add_taxonomy_filters_to_offre() {
	global $typenow;

	// an array of all the taxonomyies you want to display. Use the taxonomy name or slug
	$taxonomies = array('statuts');

	// must set this to the post type you want the filter(s) displayed on
	if( $typenow == 'projets' ){

		foreach ($taxonomies as $tax_slug) {
			$tax_obj = get_taxonomy($tax_slug);
			$tax_name = $tax_obj->labels->name;
			$terms = get_terms($tax_slug);
			if(count($terms) > 0) {
				echo "<select name='$tax_slug' id='$tax_slug' class='postform'>";
				echo "<option value=''>Afficher tous les $tax_name</option>";
				foreach ($terms as $term) {
					echo '<option value='. $term->slug, (isset($_GET[$tax_slug]) && $_GET[$tax_slug] == $term->slug) ? ' selected="selected"' : '','>' . $term->name .' (' . $term->count .')</option>';
				}
				echo "</select>";
			}
		}
	}
}
add_action( 'restrict_manage_posts', 'pippin_add_taxonomy_filters_to_offre' );*/


/*------------------------------------*\
		POLYLANG STRINGS
\*------------------------------------*/
pll_register_string("filtrer_par", "Filtrer par");
pll_register_string("partager_sur", "Partager sur");
pll_register_string("voir_aussi", "Voir aussi");
pll_register_string("en_savoir_plus", "En savoir plus");
pll_register_string("search", "Rechercher...");
pll_register_string("no_result", "Désolé, rien n'a été trouvé.");
