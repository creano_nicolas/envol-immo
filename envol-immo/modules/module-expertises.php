<section class="module scroll-reveal expertise">
	<div class="content-container">
		<?php if(!empty($module["text_title"]) || !empty($module["text_cta"])): ?>
		<div class="head-expertises">
			<h2 class="home-title-section"><?= $module["text_title"] ?></h2>
			<?php if(!empty($module["text_cta"])): ?>
			<a href="<?= $module['link_cta']; ?>" class="btn btn__invert btn-expertise-mobile"><?= $module["text_cta"] ?><i class=" btn--arrow"></i></a>
			<?php endif; ?>
		</div>
		<?php endif; ?>
		<div class="body-expertises">
			<?php foreach ($module['blocks'] as $block) : ?>
			<div class="expertises-col">
				<?php if(isset(wp_get_attachment_image_src($block["picto"], "small")[0])): ?>
				<img class="picto-expertises" src="<?= wp_get_attachment_image_src($block["picto"], "small")[0]; ?>">
				<?php endif; ?>
				<h4 class="title-smallblock-bold txt-center bold"><?= $block["title"] ?></h4>
				<p class="txt-smaller txt-center"><?= $block["text"] ?></p>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
</section>
