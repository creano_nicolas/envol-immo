<section class="module module-light-blue scroll-reveal">
	<div class="content-container">
		<h2 class="home-title-section"><?= $module["title"] ?></h2>
		<div class="body-partenaires">
			<p class="txt-smaller txt-left txt-partenaires"><?= $module["text"] ?></p>
			<a href="<?= $module['link_cta'] ?>" class="btn btn__invert mtt"><?= $module["text_cta"] ?><i class=" btn--arrow"></i></a>
		</div>
	</div>
</section>
