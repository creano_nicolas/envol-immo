<section class="module module--blue scroll-reveal" reveal-offset="300">
	<?php if (!empty($module["quote"])) : ?>
	<div class="content-container">
		<blockquote class="negatif"><?= $module["quote"] ?></blockquote>
	</div>
	<?php endif; ?>
	<div class="content-container content-container__sm">
		<div class="video-wrapper">
			<iframe src="https://www.youtube-nocookie.com/embed/<?= $module["youtube_key"] ?>" frameborder="0" color="white" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
		</div>
	</div>
</section>
