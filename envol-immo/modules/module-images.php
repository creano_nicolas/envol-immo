<section class="module <?php if (count($module["photos"]) > 1) echo "module--ct-slider";?> scroll-reveal" reveal-offset="300">
	<div class="content-container">
		<?php if (count($module["photos"]) > 1) : ?>
			<div class="ct-slider ct-slider__incontent ct-slider__s-spaces">
				<?php foreach ($module["photos"] as $photo) : ?>
					<div class="ct-slider--slide--inner">
						<img src="<?= wp_get_attachment_image_src($photo, "large")[0]; ?>">
					</div>
				<?php endforeach; ?>
			</div>
			<?php else: ?>
				<div class="ct-slider--oneslide">
					<img src="<?= wp_get_attachment_image_src($module["photos"][0], "large")[0]; ?>">
				</div>
			<?php endif; ?>
	</div>
</section>
