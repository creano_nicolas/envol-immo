<?php get_header();
$lang = pll_current_language();
$title = carbon_get_theme_option('th_membres_title_'.$lang);
$description = carbon_get_theme_option('th_membres_text_'.$lang);
?>

<div class="page-container" data-slug="membres">
    <section class="page-content">
		<div class="content-container content-container__sm">
			<div class="breadcrumb">
				<?php get_breadcrumb(); ?>
			</div>
		</div>

		<header class="page-content--header content-container content-container__sm">
			<h1 class="title-page mbl"><?= $title ?></h1>
		</header>

		<section class="content-container content-container__sm">
			<!-- Descriptif de l'équipe -->
			<p class="mbl"><?= $description ?></p>
		</section>

		<section class="content-container">
			<ul class="membersbook">

				<?php
				$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
				$post_per_page = 10000;

				$loop = new WP_Query( array(
					'post_type' => 'membres',
					'posts_per_page' => $post_per_page,
					'orderby' => 'date',
					'order' => 'ASC',
					'paged' => $paged
				) );

				$total_pages = $loop->max_num_pages;

				while ( $loop->have_posts() ) : $loop->the_post();

					$current_id = get_the_ID();

					$contentPost = [
						"imagePath" => (carbon_get_post_meta( get_the_ID(), "membre_photo") != "" ? carbon_get_post_meta( get_the_ID(), "membre_photo") : null),
						"job" => carbon_get_post_meta( get_the_ID(), "membre_job"),
						"management" => carbon_get_post_meta( get_the_ID(), "membre_management"),
						"disable_link" => carbon_get_post_meta( get_the_ID(), "disable_membre_text")
					];

					/*
					GetCarbon::get_fields(array(
						'page_default_subtitle',
						'page_header_slides',
					));

					$page_header_slide_photo = wp_get_attachment_image_src($page_header_slides[0]['page_header_slide_photo'], 'medium')[0];
					$page_header_slide_photo_alt = get_post_meta($page_header_slides[0]['page_header_slide_photo'], '_wp_attachment_image_alt', true);
					*/
					?>

					<li class="membersbook--member <?php if($contentPost['management']) echo "membersbook--member--management" ?> scroll-reveal" reveal-offset="300">
						<?php if(!$contentPost['disable_link']): ?><a href="<?= get_the_permalink(); ?>"><?php endif; ?>
							<img class="membersbook--member--photo" src="<?= $contentPost['imagePath']; ?>" alt="<?= the_title(); ?>">
							<h3 class="membersbook--member--name"><?php the_title(); ?></h3>
							<p class="membersbook--member--job"><?= $contentPost['job']; ?></p>
						<?php if(!$contentPost['disable_link']): ?></a><?php endif; ?>
					</li>

				<?php
				endwhile; ?>

			</ul>
		</section>
		<?php include(locate_template('modules/module-similar.php')); ?>
    </section>
</div>

<?php get_footer(); ?>
