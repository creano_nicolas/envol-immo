<?php get_header(); ?>
<div class="page-container" data-slug="all">
	<section class="page-content">
        <div class="content-container content-container__sm">
			<div class="breadcrumb">
				<?php get_breadcrumb(); ?>
			</div>
		</div>
		<header class="page-content--header content-container content-container__sm">
			<h1 class="title-page"><?php the_title(); ?></h1>
		</header>

		<?php
				$module = array(
					"photos" => carbon_get_the_post_meta('header_photos'),
				);
				if (count($module["photos"]) > 0){
					include(locate_template('modules/module-images.php'));
				}
		?>
		<?php include(locate_template('modules/module-share.php')); ?>
		<?php include(locate_template('templates/modules-libres.php')); ?>
		<?php include(locate_template('modules/module-similar.php')); ?>

	</section>
</div>
<?php get_footer(); ?>
