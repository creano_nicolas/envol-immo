<?php
get_header();

$current_cat_slug = get_queried_object()->slug;
$current_cat_name = get_queried_object()->name;
$lang = pll_current_language();
$title = carbon_get_theme_option('th_post_title_'.$lang);
$all = carbon_get_theme_option('th_post_all_'.$lang);
$allURL = carbon_get_theme_option('th_post_url_'.$lang);
?>

<div class="page-container" data-slug="projects">
    <section class="page-content">
        <div class="content-container content-container__sm">
			<div class="breadcrumb">
				<?php get_breadcrumb(); ?>
			</div>
		</div>

		<header class="page-content--header content-container content-container__sm">
			<h1 class="title-page"><?= $title ?></h1>
		</header>


		<section class="content-container">
			<div class="gallery-filters">
				<span><?= pll_e("Filtrer par") ?> :</span>
				<ul>
					<li><a href="<?= $allURL ?>"><?= $all ?></a></li>
				<?php
				$terms = get_terms( array(
						'taxonomy' => 'category',
						'orderby'  => 'name',
						'order'  => 'ASC',
						'hide_empty' => 1
				) );

				foreach ($terms as $term) {
					$activ_class = ($current_cat_name == $term->name) ? 'is-active' : ''; ?>

					<li class="<?= $activ_class; ?>"><a href="<?= get_term_link( $term ); ?>" data-letters="<?= $term->name; ?>"><?= $term->name; ?></a></li>

				<?php
				}
				?>

				</ul>
			</div>
		</section>

    <section class="content-container">
			<ul class="gallery">

				<?php
				$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
				$post_per_page = 10;

				$loop = new WP_Query( array(
				'post_type' => 'post',
				'posts_per_page' => $post_per_page,
		        'orderby'   => 'date',
		        'order' => 'DESC',
				'paged' => $paged,
				'tax_query' => array(
			        array(
			            'taxonomy' => 'category',
			            'field' => 'slug',
			            'terms' => $current_cat_slug
			        )
			    )
				) );

				$total_pages = $loop->max_num_pages;

				while ( $loop->have_posts() ) :
					$loop->the_post();

					$current_id = get_the_ID();

					// GetCarbon::get_fields(array(
					// 	'page_default_subtitle',
					// 	'page_header_slides',
					// ));?>


					<li class="gallery--item scroll-reveal" reveal-offset="300">
						<a href="<?= get_the_permalink(); ?>">
						<?php /* <img class="gallery--item--picture" src="<?= $page_header_slide_photo; ?>" alt="<?= $page_header_slide_photo_alt; ?>"> */ ?>
						<div class="gallery--item--picture">
							<img src="<?= wp_get_attachment_image_src(carbon_get_post_meta( get_the_ID(), "header_photos")[0], 'large')[0]; ?>" alt="<?php the_title(); ?>">
						</div>
							<h3 class="gallery--item--title"><?php the_title(); ?></h3>
						</a>
					</li>


				<?php
				endwhile; ?>

			</ul>

			<?php if(isset($total_pages) && $total_pages != 1): ?>
			<div class="pagination">
				<?php
				echo paginate_links( array(
				    'current' => max( 1, get_query_var('paged') ),
				    'total' => $total_pages,
				    'next_text' => '&raquo;',
				    'prev_text' => '&laquo;',
				    'type' => 'list'
				) );
				?>
			</div>
			<?php endif; ?>

			<?php wp_reset_query(); ?>
		</section>
    </section>
</div>

<?php get_footer(); ?>
