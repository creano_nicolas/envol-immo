<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;
use Carbon\Carbon;

/*------------------------------------*\
	HOME
\*------------------------------------*/
// HOME - événements
$home_slides = array(
	'plural_name' => 'Slides',
	'singular_name' => 'Slide',
);
Container::make( 'post_meta', __( '1/ Header' ) )
	->show_on_post_type('page')
	->show_on_template('home.php')
	->add_fields( array(
			Field::make( 'complex', 'home_header_slides', 'Slider en haut de page' )
				->set_collapsed( true )
				->set_min( 1 )
				->set_max( 4 )
				->setup_labels( $home_slides )

				->add_fields( 'home_header_slide', 'Slide', array(
					/*	Field::make( 'text', 'home_header_badge', 'Texte du badge')
							->set_width( 25 ),
		        Field::make( 'select', 'home_header_badge_theme', 'Couleur du badge' )
					    ->add_options( array(
					        'neutre' => 'Neutre',
					        'initiale' => 'Initiale',
					        'alternance' => 'Alternance',
					        'continue' => 'Continue',
					        'internationale' => 'Internationale'
					    ) )
							->set_required(true)
							->set_width( 25 ),*/
						Field::make( 'text', 'home_header_title', 'Titre du slide'),
						//	->set_width( 50 ),
						Field::make( 'text', 'home_header_button', 'Texte du bouton')
							->set_width( 50 ),
						Field::make( 'text', 'home_header_button_link', 'Lien du bouton')
							->set_width( 50 ),
						Field::make( 'select', 'home_vid_or_img', 'Video ou photo' )
						    ->add_options( array(
						        'video' => 'Vidéo',
						        'photo' => 'Photo'
						    ) )
						    ->set_required( true ),
						Field::make( 'file', 'home_header_video', 'Video')
							->set_required( true )
							->set_width( 50 )
							->set_value_type('url')
							->set_conditional_logic( array(
								array(
									'field' => 'home_vid_or_img',
									'value' => 'video',
								)
							) ),
						Field::make( 'image', 'home_header_video_img', 'Image à afficher quand la video n\'est pas en lecture')
							->set_required( true )
							->set_width( 50 )
							->set_conditional_logic( array(
								array(
									'field' => 'home_vid_or_img',
									'value' => 'video',
								)
							) ),
						Field::make( 'image', 'home_header_img', 'Image du header')
							->set_required( true )
							->set_conditional_logic( array(
								array(
									'field' => 'home_vid_or_img',
									'value' => 'photo',
								)
							) ),
				) )
				->set_header_template( '
						<% if (home_header_title) { %>
							<%- home_header_title %>
						<% } else { %>
							Slide
						<% } %>
					' ),
	) );

	Container::make( 'post_meta', __( '2/ Statistiques du groupe' ) )
	->show_on_post_type('page')
	->show_on_template('home.php')
	->add_fields( array(
		Field::make('text', 'groupe_title', 'Titre du  bloc'),
		Field::make('textarea', 'groupe_text', 'Texte du  bloc'),
		Field::make('text', 'groupe_text_cta', 'Texte du bouton (optionnel)')
			->set_width(50),
		Field::make('text', 'groupe_link_cta', 'Lien du bouton')
			->set_width(50),
		Field::make( 'separator', 'stats_sep', 'Statistiques' ),
		Field::make('text', 'groupe_number1_title', 'Type de la 1ere stat')
			->set_width(50),
		Field::make('text', 'groupe_number2_title', 'Type de la 2e stat')
			->set_width(50),
		Field::make('text', 'groupe_number1_number', 'Valeur de la 1ere stat')
			->set_width(50),
		Field::make('text', 'groupe_number2_number', 'Valeur de la 2e stat')
			->set_width(50),
		Field::make('text', 'groupe_number1_unit', 'Unités de la 1ere stat')
			->set_width(50),
		Field::make('text', 'groupe_number2_unit', 'Unités de la 2e stat')
			->set_width(50),
	) );

	Container::make( 'post_meta', __( '3/ Expertises' ) )
	->show_on_post_type('page')
	->show_on_template('home.php')
	->add_fields( array(
		Field::make('text', 'expertises_text_title', 'Texte du titre'),
		Field::make('text', 'expertises_text_cta', 'Texte du bouton (optionnel)')
			->set_width(50),
		Field::make('text', 'expertises_link_cta', 'Lien du bouton')
			->set_width(50),
		Field::make('complex', 'expertises_blocks', 'Blocs')
			->set_collapsed(true)
			->setup_labels(array('plural_name' => 'Blocs', 'singular_name' => 'Blocs'))
			->add_fields(array(
				Field::make('image', 'picto', 'type de picto' ),
				Field::make('text', 'title', 'Titre du  bloc')
					->set_width(50),
				Field::make('textarea', 'text', 'Texte du  bloc')
					->set_width(50),
			))
			->set_header_template( '<%- title %>' )
	) );


	Container::make( 'post_meta', __( '4/ Partenaires' ) )
	->show_on_post_type('page')
	->show_on_template('home.php')
	->add_fields(array(
		Field::make('text', 'partenaires_text_title', 'Titre'),
		Field::make('textarea', 'partenaires_text', 'Texte'),
		Field::make('text', 'partenaires_text_cta', 'Texte du bouton (optionnel)')
			->set_width(50),
		Field::make('text', 'partenaires_link_cta', 'Lien du bouton')
			->set_width(50),
		)
	 );

	 Container::make( 'post_meta', __( '4/ Logos' ) )
		->show_on_post_type('page')
		->show_on_template('home.php')
		->add_fields(array(
			Field::make('media_gallery', 'home_logo', 'Logos')
		));

/*------------------------------------*\
	MEMBRES
\*------------------------------------*/

Container::make("post_meta", "Détails du membre")
	->where( 'post_type', '=', 'membres' )
	->add_fields( array(
		Field::make( 'image', 'membre_photo', 'Photo' )
			->set_value_type( 'url' )
			->set_width(50),
		Field::make("text", "membre_job", "Job")
			->set_width(50),
		Field::make( 'checkbox', 'membre_management', 'Membre de la direction' )
			->set_width(50),
		Field::make( 'checkbox', 'disable_membre_text', 'Désactiver la page détail/bio' )
			->set_width(50),
	))
	->add_fields(array(
		Field::make('rich_text', 'membre_text', 'Contenus')
	));

Container::make('theme_options', __( 'Personnalisation' ) )
	->set_page_parent('edit.php?post_type=membres')
	->add_fields( array(
		Field::make( 'text', 'th_membres_title_fr', 'Titre (FR)' )
		->set_required( false )
		->set_width(50),
		Field::make( 'text', 'th_membres_title_en', 'Titre (EN)' )
		->set_required( false )
		->set_width(50),
		Field::make( 'textarea', 'th_membres_text_fr', 'Description de l\'équipe (FR)' )
			->help_text('Les retours à lignes sont pris en compte.')
			->set_required( false ),
		Field::make( 'textarea', 'th_membres_text_en', 'Description de l\'équipe (EN)' )
			->help_text('Les retours à lignes sont pris en compte.')
			->set_required( false ),
	) );

/*------------------------------------*\
	CONTACT
\*------------------------------------*/
Container::make("post_meta", "Information de contact")
	->show_on_post_type('page')
	->show_on_template('template-contact.php')
	->add_fields( array(
		Field::make('text', 'info_contact_title', 'Sous-titre'),
		Field::make('complex', 'contact_locations', 'Bureaux')
		->set_collapsed(true)
		->setup_labels(array('plural_name' => 'Blocs', 'singular_name' => 'Bureau'))
		->add_fields(array(
			Field::make('text', 'title', 'Nom du bureau')
				->set_width(50),
			Field::make('textarea', 'text', 'Informations du bureau')
				->set_width(50)
				->set_help_text("Les retours à la lignes sont pris en comptes"),
		))
		->set_header_template( '<%- title %>' ),
		Field::make('text', 'contact_form_title', 'Titre du formulaire'),
		Field::make( 'image', 'contact_img', 'Image de background'),
		Field::make('text', 'contact_cf7', 'Code du formulaire cf7'),
	));

/*------------------------------------*\
	PROJETS
\*------------------------------------*/
Container::make('theme_options', __( 'Personnalisation' ) )
	->set_page_parent('edit.php?post_type=projets')
	->add_fields( array(
        Field::make( 'text', 'th_projet_title_fr', 'Titre (FR)' )
				->set_required( false )
				->set_width(50),
				Field::make( 'text', 'th_projet_title_en', 'Titre (EN)' )
				->set_required( false )
				->set_width(50),
				Field::make( 'text', 'th_projet_all_fr', 'Text du filtre "tous les projets" (FR)' )
				->set_required( false )
				->set_width(50),
				Field::make( 'text', 'th_projet_all_en', 'Text du filtre "tous les projets" (EN)' )
				->set_required( false )
				->set_width(50),
	) );

Container::make("post_meta", "Détails du projet")
	->show_on_post_type('projets')
	->add_fields(array(
		Field::make('media_gallery', 'projet_header_photos', 'Photos')
	))
	->add_fields( array(
		Field::make('text', 'projet_text_title', 'Titre (optionnel)'),
		Field::make('textarea', 'projet_text_1', 'Contenu bloc 1')
			->set_width(50),
		Field::make('textarea', 'projet_text_2', 'Contenu bloc 2')
			->set_width(50),
		Field::make('text', 'projet_text_cta', 'Texte du bouton (optionnel)')
			->set_width(40),
		Field::make('text', 'projet_link_cta', 'Lien du bouton (optionnel)')
			->set_width(40),
		Field::make( 'checkbox', 'projet_target_cible_url', 'Ouvrir dans un nouvel onglet')
			->set_width( 20 )
			->set_option_value( 'yes' ),
		// Field::make('association', 'projet_link_cta', 'Lien du bouton')
		// 	->set_types(array(
		// 		array(
		// 			'type' => 'post',
		// 			'post_type' => 'page',
		// 		)
		// 	))
		// 	->set_max(1)
		// 	->set_width(50),
	));


/*------------------------------------*\
	PAGE.PHP
\*------------------------------------*/
Container::make( 'post_meta', __( '1/ Header' ) )
	->where('post_template', 'IN', array('default'))
	->where("post_type", "NOT IN", array('membres', 'projets'))
	->add_fields(array(
		Field::make('media_gallery', 'header_photos', 'Photos')
	));


/*------------------------------------*\
	MODULE LIBRE
\*------------------------------------*/

Container::make('post_meta', __('Contenu libre'))
	->where('post_template', 'IN', array('default'))
	->where("post_type", "NOT IN", array('membres'))
	->add_fields(array(

		Field::make('complex', 'type_de_module', 'Modules')
			->set_collapsed(true)
			->setup_labels(array('plural_name' => 'Modules', 'singular_name' => 'Modules'))

			// Module : Wysiwyg
			->add_fields('markup', 'Module WYSIWYG', array(
				Field::make('rich_text', 'text', 'Contenus')
			))

			// Module : caroussel/image
			->add_fields('images', 'Module Carrousel/Image', array(
				Field::make('media_gallery', 'photos', 'Photos')
			))

			// Module : video with quote
			->add_fields('videoquote', 'Module Video & citation', array(
				Field::make('text', 'quote', 'Citation (optionnel)'),
				Field::make('text', 'youtube_key', 'Clef vidéo youtube')
			))

			// Module : two blocs with cta
			->add_fields('twoblocks', 'Module Deux Blocs', array(
				Field::make('text', 'text_title', 'Texte du titre (optionnel)'),
				Field::make('textarea', 'text_1', 'Texte du bloc 1')
					->set_width(50),
				Field::make('textarea', 'text_2', 'Texte du  bloc 2')
					->set_width(50),
				Field::make('text', 'text_cta', 'Texte du bouton (optionnel)')
					->set_width(40),
				Field::make('text', 'link_cta', 'Lien du bouton (optionnel)')
					->set_width(40),
				Field::make( 'checkbox', 'projet_target_cible_url', 'Ouvrir dans un nouvel onglet')
					->set_width( 20 )
					->set_option_value( 'yes' ),
				// Field::make('association', 'link_cta', 'Lien du bouton')
				// 	->set_types(array(
				// 		array(
				// 			'type' => 'post',
				// 			'post_type' => 'page',
				// 		)
				// 	))
				// 	->set_max(1)
				// 	->set_width(50),
			))

			// Module : share on social media
		//	->add_fields('socialshare', 'Module Partage', array())

			// Module : mutiblocks
			->add_fields('multiblocks', 'Module Multiples Blocs', array(
				Field::make('text', 'text_title', 'Texte du titre (optionnel)'),
				Field::make('complex', 'blocks', 'Blocs')
					->set_collapsed(true)
					->setup_labels(array('plural_name' => 'Blocs', 'singular_name' => 'Blocs'))
					->add_fields(array(
						Field::make('text', 'title', 'Titre du  bloc')
							->set_width(50),
						Field::make('textarea', 'text', 'Texte du  bloc')
							->set_width(50),
					))
					->set_header_template( '<%- title %>' )
			))

			// Module : Multi blocs avec pictos (Expertises)
			->add_fields('expertise', 'Multi blocs avec pictos', array(
				Field::make('text', 'text_title', 'Texte du titre'),
				Field::make('text', 'text_cta', 'Texte du bouton (optionnel)')
					->set_width(50),
				Field::make('association', 'link_cta', 'Lien du bouton')
					->set_types(array(
						array(
							'type' => 'post',
							'post_type' => 'page',
						)
					))
					->set_max(1)
					->set_width(50),
				Field::make('complex', 'blocks', 'Blocs')
					->set_collapsed(true)
					->setup_labels(array('plural_name' => 'Blocs', 'singular_name' => 'Blocs'))
					->add_fields(array(
						Field::make('image', 'picto', 'type de picto' ),
						Field::make('text', 'title', 'Titre du  bloc')
							->set_width(50),
						Field::make('textarea', 'text', 'Texte du  bloc')
							->set_width(50),
					))
					->set_header_template( '<%- title %>' )
			))

			// Module : Partenaire (Partenaire)
			->add_fields('partenaire', 'Module Partenaire', array(
				Field::make('text', 'title', 'Titre'),
				Field::make('text', 'text', 'Texte'),
				Field::make('text', 'text_cta', 'Texte du bouton (optionnel)')
					->set_width(50),
				Field::make('text', 'link_cta', 'Lien du bouton')
					->set_width(50),
			))

			// Module : logo caroussel
			->add_fields('logocaroussel', 'Module Logos caroussel', array(
				Field::make('media_gallery', 'logo', 'Logos')
			))

	));

/*------------------------------------*\
	Footer Theme Options
\*------------------------------------*/
Container::make( 'theme_options', __( 'Footer' ) )
	->set_icon( 'dashicons-admin-customizer' )
	->add_fields( array(
		Field::make( 'text', 'footer_text_fr', 'Accroche partie gauche (FR)' )
		->set_width( 50 ),
		Field::make( 'text', 'footer_text_en', 'Accroche partie gauche (EN)' )
	 	->set_width( 50 ),
		// Field::make( 'text', 'footer_text_2', 'SousTitre' )
		// 	->set_width( 50 ),
		Field::make("text", 'footer_facebook', 'Lien Facebook')
			->set_width(25),
		Field::make("text", 'footer_twitter', 'Lien Twitter')
			->set_width(25),
		Field::make("text", 'footer_linkedin', 'Lien LinkedIn')
			->set_width(25),
		Field::make("text", 'footer_youtube', 'Lien Youtube')
			->set_width(25),
		Field::make( 'text', 'footer_copyright_fr', 'Copyright du footer (FR)' )
			->set_width( 50 ),
		Field::make( 'text', 'footer_copyright_en', 'Copyright du footer (EN)' )
			->set_width( 50 ),
	));

/*------------------------------------*\
	ARTICLES
\*------------------------------------*/
Container::make('theme_options', __( 'Personnalisation' ) )
->set_page_parent('edit.php')
->add_fields( array(
			Field::make( 'text', 'th_post_title_fr', 'Titre (FR)' )
			->set_required( false )
			->set_width(50),
			Field::make( 'text', 'th_post_title_en', 'Titre (EN)' )
			->set_required( false )
			->set_width(50),
			Field::make( 'text', 'th_post_all_fr', 'Text du filtre "toutes les actualités" (FR)' )
			->set_required( false )
			->set_width(50),
			Field::make( 'text', 'th_post_all_en', 'Text du filtre "toutes les actualités" (EN)' )
			->set_required( false )
			->set_width(50),
			Field::make( 'text', 'th_post_url_fr', 'Url page actualités (FR)' )
			->set_required( false )
			->set_width(50),
			Field::make( 'text', 'th_post_url_en', 'Url page actualités (EN)' )
			->set_required( false )
			->set_width(50),
) );

/*------------------------------------*\
	404
\*------------------------------------*/
Container::make('theme_options', __( 'Page 404' ) )
->add_fields( array(
	Field::make( 'text', 'th_404_text_fr', 'Message (FR)' )
	->set_required( false )
	->set_width(50),
	Field::make( 'text', 'th_404_text_en', 'Message (EN)' )
	->set_required( false )
	->set_width(50),
	Field::make( 'text', 'th_404_btn_fr', 'Texte du bouton "retour à l\'accueil" (FR)' )
	->set_required( false )
	->set_width(50),
	Field::make( 'text', 'th_404_btn_en', 'Texte du bouton "retour à l\'accueil" (EN)' )
	->set_required( false )
	->set_width(50),
) );
