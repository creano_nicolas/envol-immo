<?php get_header();
$lang = pll_current_language();
$title = carbon_get_theme_option('th_projet_title_'.$lang);
$all = carbon_get_theme_option('th_projet_all_'.$lang);
?>

<div class="page-container" data-slug="projects">
    <section class="page-content">
		<div class="content-container content-container__sm">
			<div class="breadcrumb">
				<?php get_breadcrumb(); ?>
			</div>
		</div>
		
		<header class="page-content--header content-container content-container__sm">
			<h1 class="title-page"><?= $title ?></h1>
		</header>

		<section class="content-container">
			<div class="gallery-filters">
				<span><?= pll_e("Filtrer par") ?> :</span>
				<ul>
					<li class="is-active"><a href="<?= get_post_type_archive_link('projets'); ?>"><?= $all ?></a></li>
					<?php
					$terms = get_terms( array(
						'taxonomy' => 'statuts',
						'orderby'  => 'name',
						'order'  => 'ASC',
						'hide_empty' => 1
					) );

					foreach ($terms as $term) { ?>
						<li><a href="<?= get_term_link( $term ); ?>" data-letters="<?= $term->name; ?>"><?= $term->name; ?></a></li>
					<?php } ?>
				</ul>
				</div>
		</section>

		<section class="content-container">
			<ul class="gallery">

				<?php
				$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
				$post_per_page = 10;

				$loop = new WP_Query( array(
					'post_type' => 'projets',
					'posts_per_page' => $post_per_page,
			        'orderby'   => 'date',
			        'order' => 'DESC',
					'paged' => $paged
				) );

				$total_pages = $loop->max_num_pages;

				while ( $loop->have_posts() ) : $loop->the_post();

					$current_id = get_the_ID();

					// var_dump($modules[$indexImg]['photos'][0]);

					// $page_header_slide_photo = wp_get_attachment_image_src($page_header_slides[0]['page_header_slide_photo'], 'medium')[0];
					// $page_header_slide_photo_alt = get_post_meta($page_header_slides[0]['page_header_slide_photo'], '_wp_attachment_image_alt', true);

					?>

					<li class="gallery--item scroll-reveal" reveal-offset="300">
						<a href="<?= get_the_permalink(); ?>">
						<?php /* <img class="gallery--item--picture" src="<?= $page_header_slide_photo; ?>" alt="<?= $page_header_slide_photo_alt; ?>"> */ ?>
						<div class="gallery--item--picture">
							<img src="<?= wp_get_attachment_image_src(carbon_get_post_meta( get_the_ID(), "projet_header_photos")[0], 'large')[0]; ?>" alt="<?php the_title(); ?>">
						</div>
						<div class="gallery--item--info">
							<h3 class="gallery--item--title"><?php the_title(); ?></h3>
						</div>
						</a>
					</li>

				<?php
				endwhile; ?>

			</ul>

			<?php if(isset($total_pages) && $total_pages != 1): ?>
			<div class="pagination">
				<?php
					echo paginate_links( array(
						'current' => max( 1, get_query_var('paged') ),
						'total' => $total_pages,
						'next_text' => '&raquo;',
						'prev_text' => '&laquo;',
						'type' => 'list'
					));
				?>
			</div>
			<?php endif; ?>

		</section>
  </section>
</div>

<?php get_footer(); ?>
