<?php get_header();
$lang = pll_current_language();
$txt = carbon_get_theme_option('th_404_text_'.$lang);
$btntxt = carbon_get_theme_option('th_404_btn_'.$lang);
?>

<div class="page-container" data-slug="404">
	<section class="page-content">
		<div class="content-container">
			<header class="page-content--header content-container content-container__sm">
				<h1 class="title-page">404</h1>
			</header>

			<div class="content-container content-container__sm">
					<p class="mts mbt"><?= $txt ?></p>
					<a href="<?php echo home_url(); ?>" class="btn mbl"><?= $btntxt ?><i class="btn--arrow"></i></a>
				</div>
			</div>
		</div>
	</section>
</div>

<?php get_footer(); ?>
