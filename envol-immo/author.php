<?php get_header(); ?>

    <div class="page-container" data-slug="author">
        <div class="content-container content-container__sm">
			<div class="breadcrumb">
				<?php get_breadcrumb(); ?>
			</div>
		</div>
        <section class="page-content">
            <h1>Auteur : <?php echo get_the_author(); ?></h1>
            <?php get_template_part('templates/loop'); ?>
            <?php get_template_part('templates/pagination'); ?>
        </section>
    </div>

<?php get_footer(); ?>