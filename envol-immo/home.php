<?php get_header(); /* Template Name: Accueil */ ?>
<div class="page-container" data-slug="all">
	<section class="page-content page-content--nomargin">
	<!---------- HERO SLIDER ---------->
		<section class="page-content__hero-banner">
			<div class="ct-slider__hero-wrapper">
				<div id="hero-slider" class="ct-slider__hero slick-css-swipe">

					<?php
					$home_header_slides = carbon_get_the_post_meta('home_header_slides');

					foreach ($home_header_slides as $slide) {
						$home_vid_or_img = $slide['home_vid_or_img'];

						if ($home_vid_or_img == "video") {
							$home_header_video = $slide['home_header_video'];
							$home_header_video_img = wp_get_attachment_image_src($slide['home_header_video_img'], 'hej_hero')[0];
							$home_header_video_img_alt = get_post_meta($slide['home_header_video_img'], '_wp_attachment_image_alt', true); ?>

							<div class="ct-slider--slide">
								<div class="ct-slider--slide--inner">
									<video class="ct-slider__hero--slide--video" poster="<?= $home_header_video_img; ?>" autoplay loop muted playsinline>
										<source src="<?= $home_header_video; ?>" type="video/mp4">
										Your browser does not support the video tag. Please update it.
									</video>
								</div>
							</div>

						<?php
							} else {
								$home_header_img = wp_get_attachment_image_src($slide['home_header_img'], 'hej_hero')[0];
								$home_header_img_alt = get_post_meta($slide['home_header_img'], '_wp_attachment_image_alt', true);    ?>

							<div class="ct-slider--slide">
								<div class="ct-slider--slide--inner">
									<img src="<?= $home_header_img; ?>" alt="<?= $home_header_img_alt; ?>">
								</div>
							</div>

					<?php
						} // endif is img or video

					} // endforeach
					?>

				</div>
				<div id="hero-slider-text" class="ct-slider__hero__text slick-css-swipe">

					<?php
					$home_header_slides = carbon_get_the_post_meta('home_header_slides');
					$count = 0;
					foreach ($home_header_slides as $slide) {
						// $home_header_badge = $slide['home_header_badge'];
						// $home_header_badge_theme = $slide['home_header_badge_theme'] == 'neutre' ? '' : 'data-theme-formation="' . $slide['home_header_badge_theme'] . '"';
						$home_header_title = $slide['home_header_title'];
						$home_header_button_link = $slide['home_header_button_link'];
						$home_header_button = $slide['home_header_button'];    ?>

						<div class="ct-slider--slide">
							<div class="ct-slider__hero__text--slide--text ">
								<div class="ct-slider__hero__text--slide--text--inner">

									<?php if (isset($home_header_title)) : ?>
										<h2 class="title-hero"><?= $home_header_title; ?></h2>
									<?php endif; ?>

									<?php if (isset($home_header_button_link) && isset($home_header_button)) : ?>
										<a href="<?= $home_header_button_link; ?>" class="btn btn__invert btn-hero">
											<span class="btn--content"><?= $home_header_button; ?></span>
											<span class="btn--arrow"></span>
										</a>
									<?php endif; ?>

								</div>
							</div>
						</div>

					<?php
						$count++;
					} // endforeach
					?>
				</div>
			</div>

			<!---------- MODULE GROUPE ---------->
			<?php
				$groupe_title = carbon_get_the_post_meta('groupe_title');
				$groupe_text = carbon_get_the_post_meta('groupe_text');
				$groupe_text_cta = carbon_get_the_post_meta('groupe_text_cta');
				$groupe_link_cta = carbon_get_the_post_meta('groupe_link_cta');
				$groupe_number1_title = carbon_get_the_post_meta('groupe_number1_title');
				$groupe_number1_number = carbon_get_the_post_meta('groupe_number1_number');
				$groupe_number1_unit = carbon_get_the_post_meta('groupe_number1_unit');
				$groupe_number2_title = carbon_get_the_post_meta('groupe_number2_title');
				$groupe_number2_number = carbon_get_the_post_meta('groupe_number2_number');
				$groupe_number2_unit = carbon_get_the_post_meta('groupe_number2_unit');
			?>
			<section class="module module--blue-groupe scroll-reveal">
				<div class="content-container container-groupe">
					<div class="groupe-textbloc">
						<h2 class="title-section-white"><?= $groupe_title ?></h2>
						<p class="txt-left"><?= $groupe_text ?></p>
						<a href="<?= $groupe_link_cta ?>" class="btn__groupe btn-expertise-mobile"><?= $groupe_text_cta ?><i class="btn--arrow"></i></a>
					</div>
					<div class="groupe-data">
						<div class="groupe-data-number">
							<h4><?= $groupe_number1_title ?></h4>
							<span><?= $groupe_number1_number ?></span>
							<p><?= $groupe_number1_unit ?></p>
							<image src="<?= get_template_directory_uri() ?>/assets/img/circle.svg" >
						</div>
						<div class="groupe-data-number">
							<h4><?= $groupe_number2_title ?></h4>
							<span><?= $groupe_number2_number ?></span>
							<p><?= $groupe_number2_unit ?></p>
							<image src="<?= get_template_directory_uri() ?>/assets/img/circle.svg" >
						</div>
					</div>
				</div>
			</section>

			<!---------- MODULE Expertises ---------->
			<?php
				$module = array(
					"text_title" => carbon_get_the_post_meta('expertises_text_title'),
					"text_cta" => carbon_get_the_post_meta('expertises_text_cta'),
					"link_cta" => carbon_get_the_post_meta('expertises_link_cta'),
					"blocks" => carbon_get_the_post_meta('expertises_blocks'),
				);
				include(locate_template('modules/module-expertises.php'));
			?>

			<!---------- MODULE partenaires ---------->
			<?php
				$module = array(
					"title" => carbon_get_the_post_meta('partenaires_text_title'),
					"text" => carbon_get_the_post_meta('partenaires_text'),
					"text_cta" => carbon_get_the_post_meta('partenaires_text_cta'),
					"link_cta" => carbon_get_the_post_meta('partenaires_link_cta'),
				);
				include(locate_template('modules/module-partenaires.php'));
			?>


			<!---------- MODULE logocaroussel ---------->
			<?php
				$module = array(
					"logo" => carbon_get_the_post_meta('home_logo'),
				);
				include(locate_template('modules/module-logocaroussel.php'));
			?>

		</section>
	</section>
</div>

<?php get_footer(); ?>
