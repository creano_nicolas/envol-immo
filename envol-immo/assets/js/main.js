/*  =============================================================================
UTILS
============================================================================== */

// _____________________________ isScrolledIntoView
// _______________________________________________________
function isScrolledIntoView(elem) {
	var docViewTop = $(window).scrollTop();
	var docViewBottom = docViewTop + $(window).height();

	var elemTop = $(elem).offset().top;

	return (elemTop <= docViewBottom);
}

// _____________________________ img on load
// _______________________________________________________
var onLoadImg = function($el, fn) {
	var loadedOnce = false;

	function launch() {
		if (!loadedOnce) {
			loadedOnce = !loadedOnce;
			fn();
		}
	}

	$el.on('load', function() {
		launch();
	}).each(function(){
		if (this.complete || this.complete === undefined){
			launch();
		}
	});
}

// _____________________________ ease
// _______________________________________________________
jQuery.extend( jQuery.easing,{
	easeInOutCubic: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t*t + b;
		return c/2*((t-=2)*t*t + 2) + b;
	}
});

// _____________________________ throttle
// _______________________________________________________
var _throttle = function(callback, delay) {
	var last;
	var timer;
	return function () {
		var context = this;
		var now = +new Date();
		var args = arguments;
		if (last && now < last + delay) {
			clearTimeout(timer);
			timer = setTimeout(function () {
				last = now;
				callback.apply(context, args);
			}, delay);
		} else {
			last = now;
			callback.apply(context, args);
		}
	};
}

// _____________________________ debounce
// _______________________________________________________
var _debounce = function(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};

// $('div:nocontent').remove();
$.expr[':'].nocontent = function(obj, index, meta, stack){
    // obj - is a current DOM element
    // index - the current loop index in stack
    // meta - meta data about your selector
    // stack - stack of all elements to loop

    // Return true to include current element
    // Return false to explude current element
    return !($.trim($(obj).text()).length) && !($(obj).children().length)
  };

// path draw
var path = function($el){
	var $path = $el;

	this.init = function() {
		$path.each(function(){
			var totalLength = this.getTotalLength();
			$(this).css({
				'stroke-dashoffset': totalLength,
				'stroke-dasharray': totalLength + ' ' + totalLength
			});
		});
	};
	this.draw = function(duration) {
		var duration = duration?duration:2;
		$path.each(function(){
			TweenMax.to($(this), duration, {strokeDashoffset: 0, ease:Power3.easeOut});
		});
	};
};

// shuffle
function shuffle(a) {
	var j, x, i;
	for (i = a.length; i; i--) {
		j = Math.floor(Math.random() * i);
		x = a[i - 1];
		a[i - 1] = a[j];
		a[j] = x;
	}
}

// reverse
jQuery.fn.reverse = [].reverse;

// toCamel
String.prototype.toCamel = function() {
	var string = this.replace(/(\-[a-z])/g, function($1) {
		return $1.toUpperCase().replace('-', '');
	});
	string = string.replace(/(\/.*)/g, '');
	string = string.replace(/(^.){1}/g, function($1) {
		return $1.toUpperCase();
	});
	return string;
};

// getHash
String.prototype.getHash = function() {
	var string = this.replace(AWBP.host, '').replace(/^\//g, '').replace(/\/$/g, '');
	if (string == '')
		string = 'home';
	return string;
}

// trimSlash
String.prototype.trimSlash = function() {
	return this.replace(/^\/+|\/+$/gm, '');
}

// trimHttp
String.prototype.trimHttp = function() {
	var text = this;
	var text = text.replace("http://", "");
	text = text.replace("https://", "");
	return text;
}

// addEndSlash
String.prototype.addEndSlash = function() {
	if (this.indexOf('?s=') != -1) {
		return this;
	} else {
		var string = this + '/';
		return string.replace(/\/\/+$/gm, '/');
	}
}

// getTranslateY
function getTranslateY(element) {
	var style = window.getComputedStyle(element.get(0));
	var matrix = style.getPropertyValue("-webkit-transform") || style.getPropertyValue("-moz-transform") || style.getPropertyValue("-ms-transform") || style.getPropertyValue("-o-transform") || style.getPropertyValue("transform");
	if (matrix === 'none') {
		matrix = 'matrix(0,0,0,0,0)';
	}
	var values = matrix.match(/([-+]?[\d\.]+)/g);
	return values[14] || values[5] || 0;
};

// Item fit Windows height
$.fn.setWindowsHeight = function(breakpoint) {
	breakpoint = breakpoint !== undefined ? breakpoint : '1025';
	var _this = this;
	this.height(window.innerHeight);
    // Fire on desktop resize
    if(window.matchMedia("(min-width: " + breakpoint + ")" ).matches) {
    	$(window).resize(function() {
    		_this.height(window.innerHeight);
    	});
    }
    // Fire on orientation change
    // Find matches
    var mql = window.matchMedia("(orientation: landscape)");
    mql.addListener(function(m) {
    	if(m.matches) {}
    		setTimeout(function(){
    			_this.height(window.innerHeight);
    		}, 500);
    });
  }

 /**
 * onCScroll
 */
 var onSmoothScroll = function(seek) {
 	$(window).on('scroll', _throttle(function(){
 		requestAnimationFrame(seek);
    }, 1000/20));// on scroll, 50 times per second, launch the function (_throttle)
 	$(window).on('scroll', _debounce(function(){
 		requestAnimationFrame(seek);
    }, 250));//250 // at the end of a scroll, launch the function after 250ms (_debounce)
 }

/*  =============================================================================
TOOLS: =
========================================================================== */
var tools = (function() {
	var isIE = IEVersion = false;

/**
 * Init tools
 */
 var init = function() {
 	bindEvents();
 }


/**
 * Bind events
 */
 var bindEvents = function() {
    // Prevent image dragging
    $('body').on('mousedown', 'img', function() { return false; });
  }


/**
 * Check if device is desktop
 */
 var isDesktop = function() {
 	return ($(window).width() >= 992);
 }

/**
 * Check if device is tablet
 */
 var isTablet = function() {
 	return ($(window).width() < 992 && $(window).width() >= 768);
 }

/**
 * Check if device is smartphone
 */
 var isSmartphone = function() {
 	return ($(window).width() < 768);
 }

/**
 * Check if device is handheld
 */
 var isTabletOrSmart = function() {
 	return (isTablet() || isSmartphone());
 }


/**
 * Detect IE
 */
 var getInternetExplorerVersion = function() {
 	var rv = -1;
 	if (navigator.appName == 'Microsoft Internet Explorer')
 	{
 		var ua = navigator.userAgent;
 		var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
 		if (re.exec(ua) != null)
 			rv = parseFloat( RegExp.$1 );
 	}
 	else if (navigator.appName == 'Netscape')
 	{
 		var ua = navigator.userAgent;
 		var re  = new RegExp("Trident/.*rv:([0-9]{1,}[\.0-9]{0,})");
 		if (re.exec(ua) != null)
 			rv = parseFloat( RegExp.$1 );
 	}
 	if (rv != -1)
 		isIE = true;

 	return rv;
 }


/**
 * Check if device is running iOs
 */
 var isIOS = function() {
 	var isIOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;

 	return isIOS;
 }

 var isAndroid = function() {
 	var ua = navigator.userAgent.toLowerCase();
    var isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
    return isAndroid;
  }

  var isWindowsPhone = function() {
  	var isWindowsPhone = false;
  	if (navigator.userAgent.match(/Windows Phone/i) || navigator.userAgent.match(/iemobile/i) || navigator.userAgent.match(/WPDesktop/i)){
  		isWindowsPhone = true;
  	}
  	return isWindowsPhone;
  }

  var isSomeRandomMobile = function() {
  	var isSomeRandomMobile = false;
  	if ( /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|playbook|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|android|silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent.toLowerCase())
  		|| /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) {
  		isSomeRandomMobile = true;
  	}
  	return isSomeRandomMobile;
  }

  var isSpecDevice = function() {
  	var isSpec = false;
  	if (isIOS() || isAndroid() || isWindowsPhone() || isSomeRandomMobile()) {
  		isSpec = true;
  	}
  	return isSpec;
  }

/**
 * Simplified version
 */
 var simplifiedVersion = function() {
 	IEVersion = getInternetExplorerVersion();

 	if (!isDesktop() || (isIE && IEVersion < 10) || isSpecDevice() || isSomeRandomMobile()) {
 		return true;
 	}
 	else {
 		return false;
 	}
 }

/**
 * Public API
 */
 return {
 	init: init,
 	isDesktop: isDesktop,
 	isTablet: isTablet,
 	isSmartphone: isSmartphone,
 	isIOS: isIOS,
 	isAndroid: isAndroid,
 	isWindowsPhone: isWindowsPhone,
 	isSomeRandomMobile: isSomeRandomMobile,
 	isSomeRandomMobile: isSomeRandomMobile,
 	simplifiedVersion: simplifiedVersion
 }
})();


/*  =============================================================================
CONTROLLER: =Site
========================================================================== */
var site = (function() {

// Vars
var slugs = [];
var currentSlug = null;
var pagesData = [];
var linkTransition = false;
var canReveal = false;


/**
 * Init site
 */
 var init = function() {
    // Tools
    tools.init();

    // Bind events
    bindEvents();

    // update Slugs - add class to <html> like 'slug--MySlug'
    updateSlug();

    // Save page data
    savePagesData(window.location.href, document.documentElement.outerHTML);

    // active popstate
    $(window).bind('popstate', historyStateChanged);

    // hide loader and show content and remove old one
    hideLoader();
    showNewContent();
    pageSwitch();

    // if simplified (smartphone, tablet...)
    if (tools.simplifiedVersion()) {
        // add class simplified
        $('html').addClass('simplifiedVersion');
      } else {
        // init smooth scroll, parallax, and awesome stuff
        initSmoothScroll();
        smoothScrollMove();
      }

    // init modules that requires being init only once
    modulesOnce();

    // Init page
    pageInit();
  }


/**
 * update Slug
 */
 var updateSlug = function() {
 	currentSlug = $('.page-container').attr('data-slug').toCamel();
 	if ($.inArray(currentSlug, slugs) < 0) {
 		slugs.push(currentSlug);
 	}
 	for (var i = slugs.length - 1; i >= 0; i--) {
 		$('html').removeClass('slug--' + slugs[i])
 	}
 	$('html').addClass('slug--' + currentSlug);
 };

/**
 * Scroll move
 */
 var smoothScrollMove = function() {
 	var container = $('.scroll-container');
 	$(window).on('scroll', function(){
 		requestAnimationFrame(function(){
 			scrollTop = $(window).scrollTop();
 			TweenLite.to(container, .75, { y:-scrollTop, ease:Power3.easeOut, force3D : true });

 			var windowHeight = $(window).height();

            // Move parallax
            var scrollElements = $('.scroll-parallax');
            if (scrollElements != null) {
            	scrollElements.each(function() {
            		var element = $(this);
            		var offsetTop = element.attr('data-top');
            		var offsetBottom = element.attr('data-bottom');

            		var level = Number(element.attr('para-strength'));
            		var amplitude = -windowHeight;
            		var movement = amplitude/(5/level);

            		if (offsetTop > (scrollTop+(windowHeight*1.3))) {
            			element.css({transform: 'translate3d(0, '+(-movement*0.5)+'px, 0)'});
            		} else if (offsetBottom < (scrollTop*0.7)) {
            			element.css({transform: 'translate3d(0, '+(movement*0.5)+'px, 0)'});
            		} else if (offsetTop < (scrollTop+(windowHeight*1.3)) && offsetBottom > (scrollTop*0.7)) {
            			var start = element.attr('data-start');
            			var stop = element.attr('data-stop');
            			var percent = (scrollTop-start)/(stop-start);
            			percent = percent-0.5;

            			var destY = movement*percent;
            			TweenLite.to(element, .75, { y:destY, ease:Power3.easeOut, force3D : true });
            		}
            	});
            }
          });
 	})
 }


/**
 * Init smooth scroll
 */
 var initSmoothScroll = function() {
 	$('.scroll-container').addClass('is-active');
 }


/**
 * History stage changed
 */
 var historyStateChanged = function() {
 	var url = window.location.href;
 	pageLoading(url, false);
 }

/**
 * smooth scroll with target
 */
 var smoothScrollTo = function(target) {
    //$('html, body').stop().animate({scrollTop: target}, 600, "easeInOutCubic");
    $('html, body').scrollTop(target);// instant
  };


/**
 * Scroll to top
 */
/*var scrollToTop = function() {
    // Scroll top
    $(window).scrollTop(0);
    if (!tools.simplifiedVersion()) {
        $('.scroll-container').css({transform: 'translate3d(0, 0px, 0)'});
    }
  }*/


/**
 * Internal link handler
 */
 var linkHandler = function(e) {
    // Get link href
    var link = $(this);
    var href = link.attr('href');
    var url = "";
    if (href !== undefined) {
    	url = href.trimSlash().trimHttp();
    }

    // return if the link has class 'no-link'
    if (link.hasClass('no-link') || href === undefined) {
    	return;
    }

    // check if the link has a hash
    if (url.charAt(0) === "#") {
    	e.preventDefault();

        // update hash on url
        // need improvements, if you want to write the hash on the url
        //location.hash = url;// working but break the site (update the url = trigger popstate = load page)

        // if the link has only "#"
        if (url.length == 1) {
        	$('body, html').scrollTop(0);
        	return;
        }

        // #someElement
        var $target = $(url)
        if (!$target.length)
        	return;
        $('body, html').scrollTop($target.offset().top);
        return;
      }

    // debug url / thishost
    //console.log(url);
    //console.log(thishost);
    //console.log(url.indexOf(thishost) === 0);

    // Check if it's an internal URL
    if (url.indexOf(thishost) === 0) {
    	e.preventDefault();

        // Get link specific transition
        /*var transition = link.data('transition');
        if (transition != undefined){
            linkTransition = transition;
        } else {
            linkTransition = false;
        }
        link.trigger('loading');*/

        // Load page
        pageLoading(url, true);
      }
    }


/**
 * Save pages data for further use
 */
 var savePagesData = function(url, data) {
 	url = url.replace(/\/$/, "").trimHttp();
 	pagesData[url] = data;
 }

/**
 * Get pages data
 */
 var getPagesData = function(url) {
 	url = url.replace(/\/$/, "").trimHttp();
 	return pagesData[url];
 }


/**
 * Load new page
 */
 var pageLoading = function(url, push) {
 	setTimeout(function(){
 		smoothScrollTo(0);
 	}, 250);
 	showLoader();
 	hideOldContent();

    // Check if data exists
    var data = getPagesData(url);

    if (data == undefined) {
    	$.get(prodVarUrl+url, function(data) {
            // Save page data
            savePagesData(prodVarUrl+url, data);

            pageCreate(data);
          }).fail(function() {
           //console.log('fail');
           //window.location = '/404';
           console.log('redirect to : ' + prodVarUrl+url);
           window.location = prodVarUrl+url;
         });
        } else {
        	pageCreate(data);
        }

        if (push) {
        	history.pushState(null, null, prodVarUrl+url.addEndSlash());
        }
      }


/**
 * Create new page
 */
 var pageCreate = function(data) {
    // Convert data to HTML
    var html = $('<div/>').html(data);

    // Create new container
    var newContainer = html.find('.page-container');
    newContainer.css({opacity: 0, position: 'absolute', top: 0, left: '-999em', width: '100%'});
    newContainer.insertBefore('.page-container:first');

    // Switch metas
    $('head meta').filter('[name="description"], [name="keywords"], [property="og:image"]').remove();
    html.find('meta').filter('[name="description"], [name="keywords"], [property="og:image"]').insertAfter('head meta[name="viewport"]');

    // Set title
    $('head title').html(html.find('title').html());

    // Switch nav (only if not in debug mode)
    $('.sitenav--nav').replaceWith(html.find('.sitenav--nav'));
    $('.wp-foot').replaceWith(html.find('.wp-foot'));

    // update Slugs - add class to <html> like 'slug--MySlug'
    updateSlug();

    // Init page
    pageInit();

    // hide loader and show content and remove old one
    hideLoader();
    showNewContent();
    pageSwitch();
  }


/**
 * Init parallax
 */
 var scrollParallaxInit = function() {
 	var scrollElements = $('.scroll-parallax');
 	scrollElements.each(function() {
 		var level = $(this).attr('data-strength');
 		if (level == undefined) {
 			var level = $(this).css('zIndex');
 			if (level == 'auto')
 				level = 1;

 			if (level > 5)
 				level = 5;

 			$(this).attr('data-strength', level);
 		} else if (level == 'rand') {
 			level = Math.random();

 			$(this).attr('data-strength', level);
 		}
 	});

 	scrollParallaxResize();
 }


/**
 * Resize parallax
 */
 var scrollParallaxResize = function() {
 	$('.scroll-parallax').each(function() {
 		var element = $(this);
 		var transform = element.css('transform');
 		element.css({transform: ''});

 		element.attr('data-top', element.offset().top);
 		element.attr('data-bottom', element.offset().top+element.outerHeight());
 		element.attr('data-start', element.offset().top-$(window).height());
 		element.attr('data-stop', element.offset().top+element.outerHeight());

 		element.css({transform: transform});
 	});
 }


/**
 * Resize handler
 */
 var resizeHandler = function() {
 	$(window).trigger('scroll');

 	if (!tools.simplifiedVersion()) {
        // Resize scroll container
        resizeContainer();

        // reset the trigger value on parallax items
        scrollParallaxResize();
      }
    }


/**
 * Resize container
 */
 var resizeContainer = function() {
    // Selectors
    var container = $('.scroll-container');
    var containerHeight = container.outerHeight();
    $('body').css({height: containerHeight});

    // Fire on orientation change
    // Find matches
    var mql = window.matchMedia("(orientation: landscape)");
    mql.addListener(function(m) {
    	setTimeout(function(){
    		$('body').css({height: window.innerHeight + 'px'	});
    	}, 800);
    });
  }


/**
 * Switch containers
 */
 var pageSwitch = function() {
 	if ($('.page-container').length > 1) {
        // Selectors
        var newContainer = $('.page-container').first();
        var oldContainer = $('.page-container').not(newContainer);

        // Switch
        newContainer.css({opacity: '', position: '', top: '', left: '', width: ''});
        oldContainer.css({opacity: 0, position: 'absolute', top: 0, left: '-999em', width: '100%'});

        $(window).trigger('resize');

        // Kill old page
        pageKill();
      } else {
      	$('.page-container').css({opacity: '', position: '', top: '', left: '', width: ''});
      }
    }


/**
 * Kill old page
 */
 var pageKill = function() {
    // Selectors
    var newContainer = $('.page-container').first();
    var oldContainer = $('.page-container').not(newContainer);

    oldContainer.remove();
  }




/**
 * Init page
 */
 var pageInit = function() {
 	if (typeof(preprod) !== 'undefined' && typeof(hostVhostLocal) !== 'undefined' && typeof(hostVhost) !== 'undefined') {
 		if (location.hostname === hostVhostLocal) {
 			$('body a[href*="'+hostVhost+'"]').each(function() {
 				var $self = $(this);
 				var currentHref = $self.attr('href');
 				var newHref = currentHref.replace(hostVhost, 'localhost:3000');
 				$self.attr('href', newHref);
 			})
 		}
 	}

    // Global things to do on page init
    $(window).trigger('resize');


    // prevent reveal stuff (so we can load the page and animate stuff before)
    // change duration according to the animation of page loading
    canReveal = false;
    durationTransition = 50;
    setTimeout(function(){
    	canReveal = true;
    	allModules();
    	$(window).trigger('scroll');
    	if (!tools.simplifiedVersion()) {
            // parallax
            scrollParallaxInit();
          }
        }, durationTransition);
  }



/*  =============================================================================
    TRANSITION PAGE
    ============================================================================== */

/**
 * Show page loader
 */
 var showLoader = function() {
 	$('.page-loader').removeClass('is-invisible');

    // Animation
    var tl = new TimelineLite();

    tl.fromTo(
    	$('.page-loader'),
    	.5,
    	{
    		alpha: 0,
    	}, {
    		alpha: 1,
    		ease: Power3.easeOut,
    	},
    	0
    	);
  }


/**
 * Hide page loader
 */
 var hideLoader = function() {
    // Animation hide loader
    var tlLoader = new TimelineLite();

    tlLoader.fromTo(
    	$('.page-loader'),
    	.5,
    	{
    		alpha: 1,
    	}, {
    		alpha: 0,
    		ease: Power3.easeIn,
    	},
    	0
    	);

    tlLoader.call(function(){
    	$('.page-loader').addClass('is-invisible');
    })
  }



/**
 * hide old content
 */
 var hideOldContent = function() {
    // Animation
    var tl = new TimelineLite();
    var Yoffset = 100;
    if($('.page-container:first').attr('data-slug') === 'home') {
    	Yoffset = 0;
    }

    tl.fromTo(
    	$('.page-container'),
    	1,
    	{
    		alpha: 1,
    		y: 0,
    	},
    	{
    		alpha: 0,
    		y: Yoffset,
    		ease:Power3.easeOut
    	},
    	0
    	);
  };


/**
 * show new content
 */
 var showNewContent = function() {
    // animation
    var tl = new TimelineLite();
    var Yoffset = 100;
    if($('.page-container:first').attr('data-slug') === 'home') {
    	Yoffset = 0;
    }


    tl.fromTo(
    	$('.page-container:first'),
    	1,
    	{
    		alpha: 0,
    		y: Yoffset,
    	},
    	{
    		alpha: 1,
    		y: 0,
    		ease:Power3.easeOut
    	},
    	0
    	);

    tl.call(function(){
    	if (!tools.simplifiedVersion()) {
            // parallax
            scrollParallaxInit();
          }
        });
  }


/*  =============================================================================
    PRELOADER (can be different than the basic loader)
    ============================================================================== */

/**
 * Show preloader
 */
 var showPreloader = function() {
    // anim introduction (different than the page transition loader)
    // ...

    // init site (at the end of the animation)
    init();
  }


/*  =============================================================================
    EVENTS - on click, scroll, resize, mousemove...
    ALWAYS BY BODY
    ============================================================================== */

    var bindEvents = function() {
    // Internal links
    $('body').on('click', 'a', linkHandler);

    // Resize handler
    $(window).on('resize orientationchange', resizeHandler);

    // reveal stuff
    $('body').on('reveal', '.scroll-reveal', scrollRevealHandler);
  }


/*  =============================================================================
    MODULES INIT - Les modules à lancer à chaque fois qu'une page est chargée
    ============================================================================== */

// modules init au lancement du site
var modulesOnce = function() {
	scrollReveal();
	burger();
	timeline();
}

// modules init à chaque nouvelle page
var allModules = function() {
	projetSeoTitle();
	heroCarousel();
	contentSlider();
	gMap();
	markupLinkAttrFx();

    // init objectFit Polyfill (from ofi.js) //
    // # allow object-fit: cover; on older browsers
    objectFitImages();

    //if (!tools.simplifiedVersion()) {}
  }


/*  =============================================================================
    MODULES
    ============================================================================== */


/**
 * Template projet : déplace les titres pour le SEO
 */
 var projetSeoTitle = function(){
 	$('.page-content--header-title-wrapper').each(function(){
 		var $el = $(this);

 		if( ($(window).width() <= 660) ) {
  		// $('.page-content--container__title').html($elContent);
  		$el.children().appendTo('.page-content--container__title');
  	}

  });
 }



/**
 * Hero carousel in home
 */
 var heroCarousel = function(){
 	$('.hero-carousel').each(function(){
 		var $el = $(this);
 		var pictureSlider =  $el.find('.hero-carousel--pictures')
 		var titleSlider = $el.find('.hero-carousel--titles')

 		var $slides = $el.children('div');

 		$el.setWindowsHeight();
        // if multiple slide
        if ($slides.length > 1) {

        	pictureSlider.on('init', function () {
        		mouseWheel(pictureSlider);
        	}).slick({
        		autoplay: true,
        		autoplaySpeed: 3000,
        		pauseOnHover: false,
        		dots: true,
        		infinite: false,
        		// speed: 1000,
        		touchThreshold: 20,
        		// swipeToSlide: true,
            // cssEase: 'cubic-bezier(0.645, 0.045, 0.355,1)', easeIn
            // cssEase: 'cubic-bezier(0,.84,.45,1)', easeOut
            // cssEase: 'cubic-bezier(0.83, 0.01, 0.31, 0.99)', slickTrack
            prevArrow:'<button class="slick-prev"></button>',
            nextArrow:'<button class="slick-next"></button>',
            asNavFor: '.hero-carousel--titles',
            lazyLoad: 'progressive',
            responsive: [
            {
            	breakpoint: 728,
            	settings: {
              		// speed: 1000,
                      //cssEase: 'linear'
                    }
                  },
                  ]
                });

            // Pause slider on interactions
            pictureSlider.find('.slick-dots, .slick-arrow').on('click', function() {
            	pictureSlider.slick('slickPause');
            });
            pictureSlider.on('swipe', function(event, slick, direction) {
            	pictureSlider.slick('slickPause');
            });

            titleSlider.slick({
            	infinite: false,
            	//slidesToShow: 1,
            	//slidesToScroll: 1,
            	arrows: false,
            	fade: true,
            	speed: 500,
            	cssEase: 'cubic-bezier(0.645, 0.045, 0.355, 1.000)',
            	asNavFor: '.hero-carousel--pictures'
            });

            setTimeout(function(){
            	$(window).trigger('resize');
            }, 1000);

          }

          function mouseWheel($slider) {
          	//$slider.on('wheel', { $slider: $slider }, mouseWheelHandler);
          }
          function mouseWheelHandler(event) {
          	event.preventDefault();
          	var $slider = event.data.$slider;
          	var delta = event.originalEvent.deltaY;
          	if (delta < 0) {
          		$slider.slick('slickPrev');
          	} else {
          		$slider.slick('slickNext');
          	}
          	pictureSlider.slick('slickPause');
          }


        });
 }

/**
 * Projects timeline
 */
 var timeline = function() {
 	if($('#projectsTimeline').length > 0) {
 		var $el = $('#projectsTimeline'),
 		button = $el.find('.timeline-ct-button'),
 		timeline = $el.find('.timeline'),
 		timelineWrapper = $el.find('.timeline-wrapper'),
 		timelineItems = $el.find('.timeline--item'),
 		timelineCircleReveal = $el.find('.timeline-circle-reveal'),
 		btnTl = new TimelineLite({paused: true}),
        // Resp
        initItemIndex = timelineItems.length/2;


        // Set timeline panel height
        $el.setWindowsHeight();
        $(window).on('scroll', _debounce(function(){
        	requestAnimationFrame(function() {
        		$el.setWindowsHeight();
        	});
        }, 250));

        // Set timeline-wrapper height
        timelineWrapper.height(timeline.outerHeight());

        // On hover anim tl button lines
        var btnHoverTl = new TimelineLite({
        	paused: true
        });
        btnHoverTl.staggerTo(button.find('.timeline-ct-button--inner').children('span'), 0.2, {x:30,ease: Power4.easeIn}, 0.02)
        .staggerTo(button.find('.timeline-ct-button--inner').children('span'), 0.3, {x:0,ease: Back.easeOut.config(0.8)}, 0.07)
        button.mouseenter( function() {
        	btnHoverTl.restart();
        });


        // Set reveal panel animation timeline
        btnTl.set($el, {className:'+=open'})
        .set(timelineCircleReveal, {autoAlpha:1})
        .to(timelineCircleReveal, 0.9, {scale:25,ease: Power4.easeInOut}, "reveal")
        .to(button, 0.3,{autoAlpha:0}, "reveal")
        .staggerTo(button.find('.timeline-ct-button--inner').children('span'), 0.2, {width:0}, 0.1, "reveal")
        .staggerFromTo($el.find('.timeline--date'), 0.8, {x:50,autoAlpha:0},{x:0,autoAlpha:0.5}, 0.4, "reveal+=0.1")
        .staggerFrom(timeline.find('.timeline--item'), 0.3, {x:10,ease:Back.easeInOut.config(1)}, 0.009, "reveal+=0.1")
        .to($el.find('.timeline-resp-close, .timeline-panel__info, .timeline-mobile-ctrl'), 0.3,{autoAlpha:1}, "reveal+=0.5")
        .set(button, {autoAlpha:0})
        if (window.matchMedia("(min-width: 768px)").matches) {
        	btnTl.call(targetTlProject)
        }

        // auto open TODO: remove
        //btnTl.restart();

        // On click reveal timeline panel
        button.click(function() {
        	if(!$el.hasClass('open')) {
        		btnTl.restart();
        		// if (window.matchMedia("(max-width: 768px)").matches) {
        		// 	$('.scroll-container').addClass('is-active');
        		// }
        		setTimeout(function() {
        			highlightItem(timelineItems, initItemIndex);
        		}, 450);
        	}
        });

        // close panel on mouse leave
        var timeoutCloseTimeline = null;
        function delayedCloseTimeline(delay) {
        	delay = delay !== undefined ? delay : 0;
        	timeoutCloseTimeline = window.setTimeout(function() {
        		$('.timeline--item').removeClass('active around around2');
                // TweenMax.to($el.find('.timeline-resp-close'), 0.3,{autoAlpha:0})
                // TweenMax.to($el.find('.timeline-panel__info'), 0.3,{autoAlpha:0})
                TweenMax.staggerTo(timeline.find('.timeline--item'), 0.3, {x:10,ease:Back.easeInOut.config(1)}, 0.009)
                TweenMax.to(button, 0.3,{autoAlpha:1}, 0.7)
                btnTl.delay(0.3).reverse().timeScale(1.1);
                // if (window.matchMedia("(max-width: 768px)").matches) {
                // 	$('.scroll-container').removeClass('is-active');
                // }
              }, delay);

            //$(window).trigger('scroll resize');
            resizeContainer();
          }
          function clearDelayedCloseTimeline() {
          	window.clearTimeout(timeoutCloseTimeline);
          }
          $el.mouseleave(function(){
          	if($el.hasClass('open')) {
          		delayedCloseTimeline(800);
          	}
          });
          $el.mouseenter(function(event) {
          	if($el.hasClass('open')) {
          		clearDelayedCloseTimeline();
          	}
          });

        // On item click/touch close timeline
        $el.find('.timeline--item--title').click(function() {
        	if($el.hasClass('open')) {
        		delayedCloseTimeline();
        	}
        });


        // Timeline itself

        // On mouseOver: display project info
        function targetTlProject() {
        	if(!$el.hasClass('open')) return
        		timelineItems.mouseenter( function() {
                //this.index;
                $('.timeline--item').removeClass('active around around2');
                $(this).addClass('active');
                var previous = $(this).prev(),
                next = $(this).next();
                previous2 = previous.prev()
                next2 = next.next()
                $(previous).addClass('around');
                $(next).addClass('around');
                $(previous2).addClass('around2');
                $(next2).addClass('around2');
              })
        }

        function highlightItem(items, index) {
        				// Round item
        				index = Math.abs(Math.round(index));

                // Get active item
                currentItem = items.eq(index);

                // If item at the end
                if(index < 0) {
                	index = 0;
                }
                if(index > items.length-1) {
                	index = items.length-1;
                }

                // Reset active items states
                items.removeClass('active around around2');

                // Make current item active
                currentItem.addClass('active');

                // Highlight items around
                var nextIndex = index+1;
                items.eq(nextIndex).addClass('around');
                items.eq(nextIndex+1).addClass('around2');

                var prevIndex = index-1;
                if(prevIndex > 0) {
                	items.eq(prevIndex).addClass('around');
                	items.eq(prevIndex-1).addClass('around2');
                }
              }



        // If mobile Device
        if (window.matchMedia("(max-width: 1024px)").matches) {

            // Close timeline button
            $('#closeTimeline').click(function() {
            	if($el.hasClass('open')) {
            		delayedCloseTimeline();
            	}
            });

            // RANGE CTRL

            var $tlCtrl = $('#timeline-ctrl');

            $tlCtrl
            .rangeslider({
            	polyfill: false
            })
            .on('input', function() {
            	highlightItem(timelineItems, this.value);
            });

            // END RANGE CTRL

            // On mobile landscape show 20 items max
            if (window.matchMedia("(max-height: 700px)").matches) {
            	var nBitemsToShowLs = 20;
            	var tlItemsLandscape = $('.timeline--item:nth-child(n+21)');
            	if($('.timeline--item:nth-child(n+21)').length <= 0) {
            		nBitemsToShowLs = timelineItems.length
            	}
            	var mql = window.matchMedia("(orientation: landscape)");
            	if(mql.matches) {
            		toggleItemsLandscape(true);
            	}
            	mql.addListener(function(m) {
            		toggleItemsLandscape(m.matches);
            	});

            	function toggleItemsLandscape(islandscape) {
            		if(islandscape) {
            			tlItemsLandscape.hide()
            			$tlCtrl.attr({
            				min: -nBitemsToShowLs+1,
            			});
            		} else {
            			$tlCtrl.attr({
            				min: -$('.timeline--item').length+1,
            			});
            			tlItemsLandscape.show();
            		}
            		$tlCtrl.rangeslider('update', true);
            	}
            }

          } // EndIf mobile Device

        // ==== */
      }
    }

/**
 * Sliders in page content
 */
 var contentSlider = function(){
 	$('.ct-slider').each(function(){

 		var $el = $(this);

        // slides
        var $slides = $el.children('div');
        // if multiple slide
        if ($slides.length > 0) {

        	$el.slick({
        		dots: true,
        		infinite: false,
        		// speed: 1000,
        		// swipeToSlide: true,
            // cssEase: 'cubic-bezier(0.645, 0.045, 0.355, 1.000)',
            // cssEase: 'cubic-bezier(0,.84,.45,1)',
            prevArrow:'<button class="slick-prev"></button>',
            nextArrow:'<button class="slick-next"></button>',
            responsive: [
            {
            	breakpoint: 728,
            	settings: {
                		// speed: 700,
                	}
                },
                ]
              });

            // if slider in header
            $el.parent('.page-content--header').addClass('page-content--header__has-slider')

          }
          setTimeout(function(){
          	$(window).trigger('resize');
          }, 1000);
        });
 }

/**
 * add attr data-letter to <a> for hover effect in .markup
 */
 var markupLinkAttrFx = function(){
 	$('.markup p > a').each(function(){
 		$(this).attr('data-letters', $(this).text());
 	});
 }


/**
 * module - burger
 */
 var burger = function() {

 	var isOpen = false;
 	var can = true;

    // on click burger or link
    $('#mobile-burger').on('click', function(){change()});
    $('body').on('click', '.sitenav--resp a', function(event) {
    	setTimeout(function(){close()},300)
    });

    var change = function() {
    	if (!can) {
    		return
    	}
    	can = false;

    	if (isOpen) {
    		close();
    	} else {
    		open();
    	}
    }

    var open = function(){
    	isOpen = true;
    	$('html').addClass('nav__active');
    	$('html').addClass('nav__show');
    	TweenMax.fromTo($('.sitenav--resp-bg'), .75, {y:'-100%'}, {y:'0%', ease:Power3.easeInOut});
    	TweenMax.fromTo($('.sitenav--resp-bg div'), .75, {y:'80%'}, {y:'0%', ease:Power3.easeInOut});
    	TweenMax.staggerFromTo($('.sitenav--resp a'), .55, {y:-100, opacity:0}, {y:0, opacity:1, ease:Power3.easeInOut, delay:0.15}, -0.025);
    	setTimeout(function(){
    		can = true;
    	}, 1000)
    }
    var close = function(){
    	isOpen = false;
    	$('html').removeClass('nav__active');
    	$('html').addClass('nav__block');
    	TweenMax.fromTo($('.sitenav--resp-bg'), .75, {y:'0%'}, {y:'-100%', ease:Power3.easeInOut});
    	TweenMax.fromTo($('.sitenav--resp-bg div'), .75, {y:'0%'}, {y:'80%', ease:Power3.easeInOut});
    	TweenMax.staggerFromTo($('.sitenav--resp a'), .55, {y:0, opacity:1}, {y:-100, opacity:0, ease:Power3.easeInOut}, 0.025);
    	setTimeout(function(){
    		$('html').removeClass('nav__show');
    		can = true;
    	}, 400)
    }
  };

/**
 * custom map with google map API
 */
 var gMap = function() {
 	if (document.getElementById('map-canvas')) {
 		$.getScript('https://maps.googleapis.com/maps/api/js?key=AIzaSyBfKbl4_brR_Xnfp6ELStym3IvD29ZeFhw&v=3.exp&sensor=false', gMapSetup)
 		function gMapSetup() {

            // google map bounds
            var bounds = new google.maps.LatLngBounds();

            // Other options for the map, pretty much selfexplanatory
            var mapOptions = {
            	zoom: 14,
                //center: new google.maps.LatLng(48.8588536,2.3119548,13),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                disableDefaultUI: true,
                zoomControl: true,
                // styles from https://snazzymaps.com/style/151/ultra-light-with-labels
                styles:[{featureType:"water",elementType:"geometry",stylers:[{color:"#e9e9e9"},{lightness:17}]},{featureType:"landscape",elementType:"geometry",stylers:[{color:"#f5f5f5"},{lightness:20}]},{featureType:"road.highway",elementType:"geometry.fill",stylers:[{color:"#ffffff"},{lightness:17}]},{featureType:"road.highway",elementType:"geometry.stroke",stylers:[{color:"#ffffff"},{lightness:29},{weight:.2}]},{featureType:"road.arterial",elementType:"geometry",stylers:[{color:"#ffffff"},{lightness:18}]},{featureType:"road.local",elementType:"geometry",stylers:[{color:"#ffffff"},{lightness:16}]},{featureType:"poi",elementType:"geometry",stylers:[{color:"#f5f5f5"},{lightness:21}]},{featureType:"poi.park",elementType:"geometry",stylers:[{color:"#dedede"},{lightness:21}]},{elementType:"labels.text.stroke",stylers:[{visibility:"on"},{color:"#ffffff"},{lightness:16}]},{elementType:"labels.text.fill",stylers:[{saturation:36},{color:"#333333"},{lightness:40}]},{elementType:"labels.icon",stylers:[{visibility:"off"}]},{featureType:"transit",elementType:"geometry",stylers:[{color:"#f2f2f2"},{lightness:19}]},{featureType:"administrative",elementType:"geometry.fill",stylers:[{color:"#fefefe"},{lightness:20}]},{featureType:"administrative",elementType:"geometry.stroke",stylers:[{color:"#fefefe"},{lightness:17},{weight:1.2}]}]
              };

            // Attach a map to the DOM Element, with the defined settings
            var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

            // Add info window
            var infowindow = new google.maps.InfoWindow();
            var marker, i;
            var count = 0;

            $('.map-infowindow').each(function() {
            	var $el = $(this)
            	var position = new google.maps.LatLng($el.attr('data-lat'), $el.attr('data-lng'));
            	bounds.extend(position);
            	marker = new google.maps.Marker({
            		position: position,
            		map: map,
            		icon: wpTemplateUrl + '/assets/img/marker.png'
            	});
            	bounds.extend(position);
            	$el.on('click', (function(marker) {
            		return function() {
            			map.panTo(marker.getPosition());
            			map.setZoom(14);
            			$('.map-infowindow').removeClass('active');
            			$el.addClass('active');
            		}
            	})(marker));
            	google.maps.event.addListener(marker, 'click', (function(marker, i) {
            		return function() {
            			map.panTo(marker.getPosition());
            			map.setZoom(14);
            			$('.map-infowindow').removeClass('active');
            			$el.addClass('active');
            			infowindow.setContent($el.prop('outerHTML'));
            			infowindow.open(map, marker);
            		}
            	})(marker, i));

                // Automatically center the map fitting all markers on the screen
                map.fitBounds(bounds);
              });

            // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
            var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {

            	if (window.matchMedia("(max-width: 768px)").matches) {
            		this.setZoom(5);
            	} else {
            		this.setZoom(6);
            	}
            	google.maps.event.removeListener(boundsListener);
            });
          }
        }

      }



/*  =============================================================================
    REVEAL (if element is in view)
    ============================================================================== */

/**
 * Scroll reveal animations
 */
 var scrollRevealHandler = function() {
 	if (!canReveal)
 		return;

 	var $el = $(this);
 	if ($el.hasClass('is-revealed'))
 		return;

 	$el.addClass('is-revealed');

    if ($el.is('.exemple-scrollreveal')) {// si mon élément est ".exemple-scrollreveal"
        // Animation
      var tl = new TimelineLite();

      tl.fromTo(
      	$el,
      	1,
      	{
      		alpha: 0,
      		y: 50,
      	}, {
      		alpha: 1,
      		y: 0,
      		ease: Power3.easeOut,
      	},
      	0
      	);
    } else if ($el.is('.foo')) {
    } else if ($el.is('.foo')) {
    } else if ($el.is('.foo')) {
    } else if ($el.is('.foo')) {
    } else if ($el.is('.foo')) {
    }
  }

  var scrollReveal = function() {
  	var seek = function(){
  		var scrollTop = $(window).scrollTop();
  		var windowHeight = $(window).height();

  		var $el = $('.scroll-reveal');
  		if ($el != null) {
  			$el.each(function() {
  				var $self = $(this);
  				if ($self.hasClass('is-revealed')) {
  					return;
  				}

  				var offset = $self.attr('reveal-offset')?parseInt($self.attr('reveal-offset')):0;

  				var offsetTop = $self.offset().top;
  				if (scrollTop + windowHeight - offset >= offsetTop) {
  					$self.trigger('reveal');
  				}
  			});
  		}
  	}
  	$(window).on('scroll', _throttle(function(){
  		requestAnimationFrame(seek);
    }, 1000/20));// on scroll, 50 times per second, launch the function (_throttle)
  	$(window).on('scroll', _debounce(function(){
  		requestAnimationFrame(seek);
    }, 250));// at the end of a scroll, launch the function after 250ms (_debounce)
  };




/**
 * Public API
 */
 return {
 	showPreloader: showPreloader
 }
})();





// host and url
var prodVarUrl = 'http://';
if (location.protocol == 'https:') {
prodVarUrl = 'https://';
}
// on recup le host en php avec get_site_url() et on cut apres '://'
var thishost = wpSiteUrl.split('://').pop(); //'127.0.0.1/chambre-et-vibert';// host prod



/* preprod only */
var preprod = false;
var hostPreprod = 'wp.eric-huguenin.com';

//var hostVhost = 'wp.dev';
var hostVhost = '127.0.0.1';
var hostVhostLocal = 'localhost';


if (location.hostname === hostPreprod) {// host preprod
	thishost = hostPreprod;
}

if (location.hostname === hostVhost) {// vhost preprod
	thishost = hostVhost;
}

if (location.hostname === hostVhostLocal) {// localhost
    thishost = hostVhostLocal+':3000';// localhost:3000
  }
  /* end - preprod only */


// Launch site
site.showPreloader();
