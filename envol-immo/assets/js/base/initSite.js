// utils
import sniffer from '../utils/sniffer.js'

// base
import ajax from './ajax.js'
import initPage from './initPage.js'

// scroll reveal
import scrollReveal from '../scroll/scrollReveal.js'

// scroll reveal
import { initSmoothScroll } from '../scroll/smoothScroll.js'
import { siteModules } from '../modules/modules.js'


function initSite() {
  // prevent reveal stuff
  window.canReveal = false

	initSmoothScroll();

	if(window.startOptions.ajax){
		// ajax
		ajax();
	}


  // prevent drag on image
  $('body').on('mousedown', 'img', function() { return false; });

  // reveal
	scrollReveal();

  // init page
  initPage()

	//MODULE
	siteModules();
}


export default initSite
