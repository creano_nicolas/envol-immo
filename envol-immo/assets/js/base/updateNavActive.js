function updateNavActive() {
	let currentURL = document.URL.trimSlash().trimHttp();

	let $link = $("a");

	$link.each(function() {
		let $self = $(this);
		$self.removeClass("current-link");
		let href = $self
			.attr("href")
			.trimSlash()
			.trimHttp();
		if (href === currentURL) $self.addClass("current-link");
	});

	$(".menu-list>li").each(function() {
		$(this).removeClass("current-li");
		if($(this).find(".current-link").length){
			$(this).addClass("current-li");
		}
	});
}

export default updateNavActive;
