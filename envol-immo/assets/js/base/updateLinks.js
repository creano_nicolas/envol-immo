function updateLinks() {
	let protocol = location.protocol;
	let hostname = location.hostname;
	let port = location.port || "";
	let dotsport = port.length ? ":" + port : "";

	$('a[href^="/"]')
		.add('a[href^="./"]')
		.each(function() {
			let $self = $(this);
			var href = $self
				.attr("href")
				.trimSlash()
				.trimHttp();
			let buildHref = `${protocol}//${hostname}${dotsport}/${href}`;
			$self.attr("href", buildHref);
		});

	/*$( 'a' ).each(function() {
    if( location.hostname === this.hostname || !this.hostname.length ) {
        $(this).addClass('local');
    } else {
        $(this).addClass('external');
    }
  });*/
}

export default updateLinks;
