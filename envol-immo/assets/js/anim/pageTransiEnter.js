// scroll
import scroll from "../scroll/scroll.js";
import sniffer from "../utils/sniffer.js";

function pageTransiEnter() {
	// timeline
	let tl = new TimelineLite();

	tl.to($(".global-container"), 0.5, { alpha: 1 }, 0);

}

export default pageTransiEnter;
