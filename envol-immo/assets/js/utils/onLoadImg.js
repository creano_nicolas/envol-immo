import sniffer from "./sniffer.js"

function onLoadImg($el, fn) {
  let loadedOnce = false;
  function launch() {
    if (!loadedOnce) {
      loadedOnce = !loadedOnce;
      fn();
    }
  }

  $el.on('load error', e => {
    launch();
  }).each(function(){
    if (sniffer.isIE) {
      launch();
    } else {
      if (this.complete || this.complete === undefined){
        launch();
      }
    }
  });
}

export default onLoadImg;
