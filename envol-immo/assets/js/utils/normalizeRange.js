// Got this from Josh Kirk
// https://twitter.com/joshgkirk?lang=fr

// idea is to normalize any number between ranges

function normalizeRange (value, originalMin, originalMax, newMin, newMax)  {
  const originalRange = originalMax - originalMin;
  const newRange = newMax - newMin;
  return (((value - originalMin) * newRange) / originalRange) + newMin;
};

export default normalizeRange;

/*
example :
value is 0 to 1 (like perone of scroll position)
originalMin is 0 (min range of value)
originalMax is 1 (max range of value)
newMin is the minimum wanted value
newMax is the maximum wanted value
normalizeRange(0,    0, 1, -100, 100) // -100
normalizeRange(0.25, 0, 1, -100, 100) // -50
normalizeRange(0.5,  0, 1, -100, 100) // 0
normalizeRange(0.75, 0, 1, -100, 100) // 50
normalizeRange(1,    0, 1, -100, 100) // 100
*/