import sniffer from "../utils/sniffer.js";
import {_throttle} from "../utils/utils.js";

function initSmoothScroll() {
	if (window.startOptions.smoothscroll && !sniffer.isSimplified) {
		$(".scroll-container").addClass("is-active");
		var container = $(".scroll-container");
		setInterval(resizeContainer, 500);

		$(window).on(
			"scroll",
			_throttle(function() {
				requestAnimationFrame(function() {
					var scrollTop = $(window).scrollTop();
					TweenLite.to(container, 1, {
						y: -scrollTop,
						ease: Power3.easeOut,
						force3D: true
					});
					var windowHeight = $(window).height();
				});
			}, 0)
		);
	}
}

let oldheight = 0;
function resizeContainer() {
	setTimeout(function() {

	// Selectors
	var container = $(".scroll-container");
	var containerHeight = container.outerHeight();
	if(oldheight == containerHeight){
		return;
	}
	oldheight = containerHeight;
	$("body").css({ height: containerHeight });

	// Fire on orientation change
	// Find matches
	var mql = window.matchMedia("(orientation: landscape)");
	mql.addListener(function(m) {
		setTimeout(function() {
			$("body").css({ height: window.innerHeight + "px" });
		}, 800);
	});

}, 500);
}

export { initSmoothScroll, resizeContainer };
