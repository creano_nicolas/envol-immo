// utils
import {preventDefault, preventDefaultForScrollKeys, disableScroll, enableScroll} from '../utils/disableScroll.js'

let scroll = {

  disable () {
    disableScroll()
    $('html').addClass('scroll-disable')
  },

  enable () {
    enableScroll()
    $('html').removeClass('scroll-disable')
  },

  to (val) {
    TweenMax.to('html, body', 1, {scrollTop:val, ease:Power3.easeInOut})
  }

}

export default scroll
