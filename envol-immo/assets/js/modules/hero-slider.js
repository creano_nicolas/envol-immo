function heroslider() {

	// Hero slider
	$('#hero-slider').each(function () {
		var $el = $(this);
		// slides
		var $slides = $el.children('div');
		// if multiple slide
		if ($slides.length > 1) {

			// init slide counter
	//		site.sliderNbIndic($el, $heroSliderNbIndic);

			// On slider init
			$el.on('init reInit', function () {
				if ($el.hasClass('slick-initialized') && $el.find('.ct-slider__hero--slide--video').length) {
					// play videos in slides
					$el.find('.ct-slider__hero--slide--video')[0].play();
				}
			});

			// Setup slider
			$el.slick({
				autoplay: true,
				autoplaySpeed: 10000,
				dots: false,
				infinite: true,
				speed: 1000,
				pauseOnHover: false,
				swipeToSlide: true,
				cssEase: 'cubic-bezier(0,.84,.45,1)',
				prevArrow: '<button class="slick-prev"></button>',
				nextArrow: '<button class="slick-next"></button>',
				asNavFor: '#hero-slider-text',
				// lazyLoad: 'progressive',
				responsive: [{
					breakpoint: 728,
					settings: {
						speed: 700,
					}
				},]
			});

			// if slider in header
			$el.parents('.page-content__hero-banner').addClass('has-slider');

		}
		setTimeout(function () {
			$(window).trigger('resize');
		}, 1000);
	});
	// Hero Slider text
	$('#hero-slider-text').each(function () {

		var $el = $(this);

		// slides
		var $slides = $el.children('div');
		// if multiple slide
		if ($slides.length > 1) {
			// Setup slider
			$el.slick({
				dots: false,
				arrows: false,
				infinite: true,
				adaptiveHeight: true,
				speed: 1000,
				pauseOnHover: false,
				swipeToSlide: true,
				cssEase: 'cubic-bezier(0,.84,.45,1)',
				asNavFor: '#hero-slider',
				// lazyLoad: 'progressive',
				responsive: [{
					breakpoint: 728,
					settings: {
						speed: 700,
					}
				},]
			});
		}

		setTimeout(function () {
			$(window).trigger('resize');
		}, 1000);
	});
}


export default heroslider;
