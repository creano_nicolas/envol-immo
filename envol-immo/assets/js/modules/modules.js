import ctslider from "./ct-slider.js";
import {fwmbMenu, fwmbMenuClose} from "./fwmbMenu.js";
import logoslider from "./logoslider.js";
import heroslider from "./hero-slider.js";
import search from "./search.js";


//GLOBAL AU SITE GENRE LE BURGER
function siteModules() {
	fwmbMenu();
	search();
}

//PAGE SPECIFIQUE
function pageModules() {
	heroslider();
	ctslider();
	logoslider();
	fwmbMenuClose();
}

export { siteModules, pageModules };
