function fwmbMenu() {

	// Hide mobile submenus
	$('#fwmb-menu').find('.submenu').hide();

	// burger actions
	$('#fwmb-trigger').on('click', function(){
		$('body').toggleClass('has-fwmbmenuopen');
		$('#fwmb-menu').toggleClass('fwmb-menuopen');
	});

	// responsive nav - accordion on subnav
	$('body').on('click touchend', '.item-link-arrow', function(){
		// if (responsiveNavActive) {
			$(this).next('.submenu').slideToggle(200);
			$(this).parents('.menu-item-has-children').toggleClass('active');
		// }
		return false;
	});

}

function fwmbMenuClose() {
	$('body').removeClass('has-fwmbmenuopen');
	$('#fwmb-menu').removeClass('fwmb-menuopen');
}

export {fwmbMenu, fwmbMenuClose};
