function search(){
	$("#search_icon").click(function(){
		console.log("test");
		$(".sitenav-searchbar").fadeIn(200);
		$(".search-input").focus();// focus on input to prevent a click
	});
	$(".sitenav-searchbar__close").click(function(){
		$(".sitenav-searchbar").fadeOut(200);
		$(".search-input").blur();// lose focus on input because it's not active anymore
	});
}

export default search;
