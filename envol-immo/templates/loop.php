<?php if (have_posts()): while (have_posts()) : the_post(); ?>

	<?php get_template_part('templates/card'); ?>

<?php endwhile; ?>
<?php else: ?>

	<section class="page-content--section">
		<div class="content-container content-container__sm">
			<div class="markup">
				<p><?php pll_e("Désolé, rien n'a été trouvé."); ?></p>
			</div>
		</div>
	</section>

<?php endif; ?>