<?php

// Module Statique

// include(locate_template('modules/module-carousselheader.php'));

/*include(locate_template('modules/module-groupe.php'));

include(locate_template('modules/module-expertises.php'));

include(locate_template('modules/module-partenaires.php'));

include(locate_template('modules/module-logocaroussel.php'));*/

$modules = carbon_get_the_post_meta('type_de_module');
foreach ($modules as $module) {
	switch ($module['_type']) {
			// case 'headerslider':
			// 	include(locate_template('modules/module-carousselheader.php'));
			// 	break;
		case 'expertise':
			include(locate_template('modules/module-expertises.php'));
			break;
		case 'markup':
			include(locate_template('modules/module-markup.php'));
			break;
		case 'images':
			include(locate_template('modules/module-images.php'));
			break;
		case 'videoquote':
			include(locate_template('modules/module-videowithquote.php'));
			break;
		case 'twoblocks':
			include(locate_template('modules/module-twoblocks.php'));
			break;
	/*	case 'socialshare':
			include(locate_template('modules/module-share.php'));
			break;*/
		case 'multiblocks':
			include(locate_template('modules/module-multiblocks.php'));
			break;
		case 'logocaroussel':
			include(locate_template('modules/module-logocaroussel.php'));
			break;
		case 'partenaire':
			include(locate_template('modules/module-partenaires.php'));
			break;
		case 'logocaroussel':
			include(locate_template('modules/module-logocaroussel.php'));
			break;
	}
}
