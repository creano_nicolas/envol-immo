<?php
$secteurs_slug = array();
$terms = get_the_terms($post->ID, "secteurs");

if($terms){

	foreach ($terms as $term) {

		array_push($secteurs_slug, $term->slug);

	}

}

$newloop = new WP_Query( array(
'post_type' => 'projets',
'posts_per_page' => 2,
'post__not_in' => array(get_the_ID()),
'tax_query' => array(
    array(
        'taxonomy' => 'secteurs',
        'field' => 'slug',
        'terms' => $secteurs_slug
    )
)
) );

if(sizeof($newloop->posts) > 0): ?>

	<section>
		<div class="page-content--container">
			<h2 class="section-title">Projets similaires</h2>
		</div>
		<ul class="gallery">

			<?php
			if (have_posts()): while ( $newloop->have_posts() ) : $newloop->the_post();

				GetCarbon::get_fields(array(
					'page_default_subtitle',
					'page_header_slides',
				));

				$page_header_slide_photo = wp_get_attachment_image_src($page_header_slides[0]['page_header_slide_photo'], 'medium')[0];
				$page_header_slide_photo_alt = get_post_meta($page_header_slides[0]['page_header_slide_photo'], '_wp_attachment_image_alt', true); ?>

				<li class="gallery--item">
					<a href="<?= get_the_permalink(); ?>">
						<img class="gallery--item--picture" src="<?= $page_header_slide_photo; ?>" alt="<?= $page_header_slide_photo_alt; ?>">
					</a>
					<div class="gallery--item--info">
						<h3 class="gallery--item--title"><?php the_title(); ?></h3>
						<p class="gallery--item--subtitle"><?= $page_default_subtitle; ?></p>
					</div>
				</li>

			<?php
			endwhile;
			endif;
			wp_reset_query();
			?>

		</ul>
	</section>

<?php endif; ?>
