<?php
    if ( has_post_thumbnail()) :
        $class = 'card card__vign';
    else :
        $class = 'card';
    endif;
?>

<!-- card -->
<article id="card-<?php the_ID(); ?>" <?php post_class($class); ?>>
    <div class="card--cont">
        <?php if ( has_post_thumbnail()) : ?>
            <div class="card--thumb">
                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                    <?php the_post_thumbnail(array(400, 400)); ?>
                </a>
            </div>
        <?php endif; ?>
        <div class="card--content">
            <h2 class="card--title">
                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
            </h2>
            <div class="card--infos">
                <span class="card--date">Publié le <?php the_time('j/m/Y'); ?></span>
                <span class="card--author">Par <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php the_author_meta('user_firstname'); ?></a></span>
            </div>
            <div class="card--excerpt">
                <?php html5wp_excerpt('html5wp_index'); ?>
            </div>
        </div>
    </div>
    <?php //edit_post_link(); ?>
</article>
<!-- /card -->