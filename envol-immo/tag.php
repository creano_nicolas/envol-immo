<?php get_header(); ?>

    <div class="page-container" data-slug="tag">
        <section class="page-content">
            <h1>Archives : <?php echo single_tag_title('', false); ?></h1>
            <?php get_template_part('templates/loop'); ?>
            <?php get_template_part('templates/pagination'); ?>
        </section>
    </div>

<?php get_footer(); ?>