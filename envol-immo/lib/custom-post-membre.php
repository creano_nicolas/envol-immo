<?php

/* Custom Post Membres
*******************/
function custom_post_type_membres() {

// Set UI labels for Custom Post Type
	$labels = array(
		'name'                => _x( 'Membres', 'Post Type General Name', 'envol-immo' ),
		'singular_name'       => _x( 'membre', 'Post Type Singular Name', 'envol-immo' ),
		'menu_name'           => __( 'Membres', 'envol-immo' ),
		'parent_item_colon'   => __( 'membre', 'envol-immo' ),
		'all_items'           => __( 'Tous les membres', 'envol-immo' ),
		'view_item'           => __( 'Voir le membre', 'envol-immo' ),
		'add_new_item'        => __( 'Ajouter un membre', 'envol-immo' ),
		'add_new'             => __( 'Ajouter', 'envol-immo' ),
		'edit_item'           => __( 'Editer le membre', 'envol-immo' ),
		'update_item'         => __( 'Modifier le membre', 'envol-immo' ),
		'search_items'        => __( 'Chercher un membre', 'envol-immo' ),
		'not_found'           => __( 'Non trouvé', 'envol-immo' ),
		'not_found_in_trash'  => __( 'Non trouvé dans la corbeille', 'envol-immo' ),
	);

// Set other options for Custom Post Type

	$args = array(
		'label'               => __( 'membres', 'envol-immo' ),
		'description'         => __( 'Permet de gérer les membres', 'envol-immo' ),
		'labels'              => $labels,
		'supports'            => array( 'title' ),
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 30,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
		'menu_icon' => 'dashicons-groups',
		'menu_position' => 6
	);

	// Registering your Custom Post Type
	register_post_type( 'membres', $args );

}

/* Hook into the 'init' action so that the function
* Containing our post type registration is not
* unnecessarily executed.
*/

add_action( 'init', 'custom_post_type_membres', 0 );