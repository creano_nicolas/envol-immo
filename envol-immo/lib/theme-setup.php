<?php

/*
 * Add page slug to body class, love this - Credit: Starkers Wordpress Theme
 */

/*
function add_slug_to_body_class($classes)
{
  global $post;
  if (is_home()) {
    $key = array_search('blog', $classes);
    if ($key > -1) {
      unset($classes[$key]);
    }
  } elseif (is_page()) {
    $classes[] = sanitize_html_class($post->post_name);
  } elseif (is_singular()) {
    $classes[] = sanitize_html_class($post->post_name);
  }
  return $classes;
}
add_filter('body_class', 'add_slug_to_body_class');
*/

/*
 * Custom Excerpts
 * Create 20 Word Callback for Index page Excerpts, call using html5wp_excerpt('html5wp_index');
 */

function html5wp_index($length)
{
  return 20;
}

/*
 * Create 40 Word Callback for Custom Post Excerpts.
 * Call using html5wp_excerpt('html5wp_custom_post');
 */

function html5wp_custom_post($length)
{
  return 40;
}

/*
 * Create the Custom Excerpts callback
 */

function html5wp_excerpt($length_callback = '', $more_callback = '')
{
  global $post;
  if (function_exists($length_callback)) {
    add_filter('excerpt_length', $length_callback);
  }
  if (function_exists($more_callback)) {
    add_filter('excerpt_more', $more_callback);
  }
  $output = get_the_excerpt();
  $output = apply_filters('wptexturize', $output);
  $output = apply_filters('convert_chars', $output);
  $output = '<p>' . $output . '</p>';
  echo $output;
}

/*
 * Custom View Article link to Post
 */

function html5_blank_view_article($more)
{
  global $post;
  return '… <a class="article-viewMore" href="' . get_permalink($post->ID) . '">' . __('Lire l\'article', 'wpblank') . '</a>';
}
add_filter('excerpt_more', 'html5_blank_view_article');

/*
 * Remove 'text/css' from our enqueued stylesheet
 */

function html5_style_remove($tag)
{
  return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}
add_filter('style_loader_tag', 'html5_style_remove');

/*
 * Remove wp_head() injected Recent Comment styles
 */

function my_remove_recent_comments_style()
{
  global $wp_widget_factory;
  remove_action('wp_head', array(
    $wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
    'recent_comments_style'
  ));
}

/*
 * No Duplicate content
 * http://www.geekpress.fr/wordpress/astuce/duplicate-content-categorie-1416/
 */

function baw_non_duplicate_content( $wp )
{
  global $wp_query;
  if( isset( $wp_query->query_vars['category_name'], $wp_query->query['category_name'] )
  && $wp_query->query_vars['category_name'] != $wp_query->query['category_name'] ) :
  $correct_url = str_replace( $wp_query->query['category_name'], $wp_query->query_vars['category_name'], $wp->request );
    wp_redirect( home_url( $correct_url ), 301 );
  die();
  endif;
}
add_action('wp', 'baw_non_duplicate_content' );

/*
 * Remove WordPress custom field
 */

function baw_remove_custom_field_meta_boxes()
{
  remove_post_type_support( 'post','custom-fields' );
  remove_post_type_support( 'page','custom-fields' );
}
add_action('init','baw_remove_custom_field_meta_boxes');

/*
 * Remove Widget Calendar
 */

function remove_calendar_widget()
{
  unregister_widget('WP_Widget_Calendar');
}
add_action( 'widgets_init', 'remove_calendar_widget' );

/*
 * Load theme textdomain
 */

function my_theme_setup(){
  load_theme_textdomain('my_theme', get_template_directory() . '/languages');
}
add_action('after_setup_theme', 'my_theme_setup');



/*
 * @Get id by template name
 */
function get_id_by_template_name($template_name){

	$args = array(
    'post_type' => 'page',
    'fields' => 'ids',
    'nopaging' => true,
    'meta_key' => '_wp_page_template',
    'meta_value' => $template_name
	);
	$pages = get_posts( $args );

	if( sizeof($pages) <= 0)
		return false;

	$page_id = $pages[0];
	return $page_id;

}
