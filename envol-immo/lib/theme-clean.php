<?php

/* Add Filters
-------------------------- */

// Allow shortcodes in Dynamic Sidebar
add_filter('widget_text', 'do_shortcode');

// Remove <p> tags in Dynamic Sidebars (better!)
add_filter('widget_text', 'shortcode_unautop');

// Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'shortcode_unautop');

// Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'do_shortcode');


// Remove Admin bar
function remove_admin_bar()
{
    return false;
}
add_filter('show_admin_bar', 'remove_admin_bar');

// remove useless emoji
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

/* Remove Filters
-------------------------- */

// Remove <p> tags from Excerpt altogether
remove_filter('the_excerpt', 'wpautop');
