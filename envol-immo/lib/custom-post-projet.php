<?php

/* Custom Post Projets
*******************/
function custom_post_type_projets() {

// Set UI labels for Custom Post Type
	$labels = array(
		'name'                => _x( 'Projets', 'Post Type General Name', 'envol-immo' ),
		'singular_name'       => _x( 'projet', 'Post Type Singular Name', 'envol-immo' ),
		'menu_name'           => __( 'Projets', 'envol-immo' ),
		'parent_item_colon'   => __( 'projet', 'envol-immo' ),
		'all_items'           => __( 'Tous les projets', 'envol-immo' ),
		'view_item'           => __( 'Voir le projet', 'envol-immo' ),
		'add_new_item'        => __( 'Ajouter un projet', 'envol-immo' ),
		'add_new'             => __( 'Ajouter', 'envol-immo' ),
		'edit_item'           => __( 'Editer le projet', 'envol-immo' ),
		'update_item'         => __( 'Modifier le projet', 'envol-immo' ),
		'search_items'        => __( 'Chercher un projet', 'envol-immo' ),
		'not_found'           => __( 'Non trouvé', 'envol-immo' ),
		'not_found_in_trash'  => __( 'Non trouvé dans la corbeille', 'envol-immo' ),
	);

// Set other options for Custom Post Type

	$args = array(
		'label'               => __( 'projets', 'envol-immo' ),
		'description'         => __( 'Permet de gérer les projets', 'envol-immo' ),
		'labels'              => $labels,
		'supports'            => array( 'title' ),
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 30,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
		'menu_icon' => 'dashicons-admin-multisite',
		'menu_position' => 5
	);

	// Registering your Custom Post Type
	register_post_type( 'projets', $args );

}

/* Hook into the 'init' action so that the function
* Containing our post type registration is not
* unnecessarily executed.
*/

add_action( 'init', 'custom_post_type_projets', 0 );

/* Custom Taxonomy
*******************/
register_taxonomy(
	'statuts',
	'projets',
	array(
		'label' => 'Statuts',
		'show_admin_column' => true,
		'labels' => array(
			'name' => 'Statuts',
			'singular_name' => 'Statut',
			'all_items' => 'Tous les statuts',
			'edit_item' => 'Éditer le statut',
			'view_item' => 'Voir le statut',
			'update_item' => 'Mettre à jour le statut',
			'add_new_item' => 'Ajouter un statut',
			'new_item_name' => 'Nouvelle statut',
			'search_items' => 'Rechercher parmi les statuts',
			'popular_items' => 'Statut les plus utilisés'
		),
		'hierarchical' => true
	)
);
register_taxonomy_for_object_type( 'statuts', 'projets' );