<?php

/*
 * Add Scripts
 */

function html5blank_header_scripts()
{
  if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {
    // jQuery
    wp_deregister_script('jquery');
    wp_register_script('jquery', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js", false, null, true);
    wp_enqueue_script('jquery');

    // Custom scripts
    wp_register_script('html5blankscripts', get_template_directory_uri() . '/assets/js/scripts.js', array('jquery'), '1.0.0', true);
    wp_enqueue_script('html5blankscripts');
  }
}
//add_action('init', 'html5blank_header_scripts');


/*
 * Remove wp-embed-min.js
 */

function my_deregister_scripts(){
  wp_deregister_script( 'wp-embed' );
}
add_action( 'wp_footer', 'my_deregister_scripts' );


/*
 * Remove ?ver=x.x from css and js
 */

function remove_cssjs_ver( $src )
{
  if( strpos( $src, '?ver=' ) )
  $src = remove_query_arg( 'ver', $src );
  return $src;
}


/*
 * Email encoding
 */
function encode_email($e) {
	$output = '';
	for ($i = 0; $i < strlen($e); $i++) {
		$output .= '&#'.ord($e[$i]).';';
	}
	return $output;
}


/*
 * Custom print function
 */
function pm($element){
	echo '<pre>';
	print_r($element);
	echo '</pre>';
}

/**
 * Clean display for mac
 */
function print_mac($array = array()){
	echo '<pre>';
	print_r($array);
	echo '</pre>';
}

/*
 * Add Conditional Page Scripts
 */

//function html5blank_conditional_scripts()
//{
//  if (is_page('pagenamehere')) {
//    wp_register_script('scriptname', get_template_directory_uri() . '/js/scriptname.js', array('jquery'), '1.0.0'); // Conditional script(s)
//    wp_enqueue_script('scriptname'); // Enqueue it!
//  }
//}
//add_action('wp_print_scripts', 'html5blank_conditional_scripts');
