<?php
/*
 * REST API
 *
 * Disable rest api access instead wp admin
 */
// add_filter( 'rest_authentication_errors', function( $result ) {
//     if ( ! empty( $result ) ) {
//         return $result;
//     }
//     if ( ! is_user_logged_in() ) {
//         return new WP_Error( 'rest_not_logged_in', 'You are not currently logged in.', array( 'status' => 401 ) );
//     }
//     return $result;
// });
// => do we have to add a condition for role admin only?


/*
 * XML RPC
 *
 * Disable xml rpc
 */
add_filter( 'xmlrpc_enabled', '__return_false' );
remove_action( 'wp_head', 'rsd_link' );

// Or via htaccess # Block WordPress xmlrpc.php requests
// <Files xmlrpc.php>
// order deny,allow
// deny from all
// allow from 123.123.123.123
// </Files>
