<?php

/*
 * Add styles
 */

function html5blank_styles()
{
  // Styles
  wp_register_style('wpblank', get_template_directory_uri() . '/assets/css/style.css', array(), '1.0', 'all');
  wp_enqueue_style('wpblank');
}
add_action('wp_enqueue_scripts', 'html5blank_styles');
