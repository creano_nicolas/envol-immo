<?php
wp_reset_postdata();
// Get les fields du theme option "Footer"
$lang = pll_current_language();
$footerField = [
		"titre" => carbon_get_theme_option('footer_text_'.$lang),
		"socialLinks" => [
		"facebook" => carbon_get_theme_option('footer_facebook'),
		"twitter" => carbon_get_theme_option('footer_twitter'),
		"linkedin" => carbon_get_theme_option('footer_linkedin'),
		"youtube" => carbon_get_theme_option('footer_youtube'),
	],
	"copyrghtText" => carbon_get_theme_option('footer_copyright_'.$lang)
];
?>

	<footer>
		<div class="footer__upper">
			<div class="footer__social-section">
				<p><?= $footerField["titre"]; ?></p>
				<div class="footer__social-link">
					<?php
						/**
						 * L'indice du tableau sera utilisé pour ciblé un svg en particulier
						 * puisque l'indice est identique au nom du svg
						 */
						foreach($footerField["socialLinks"] as $key => $link) :
							if($link != "") :
					?>
								<a href="<?= htmlentities($link); ?>" target="_blank"><?php htmlentities(get_template_part( 'assets/svg/'.$key )); ?></a>
					<?php endif; endforeach; ?>
				</div>
			</div>
			<div class="footer__link-section">
				<div class="footer__nav">
					<?php nav_footer(); ?>
				</div>
				<div class="footer__links">
					<?php nav_footer_secondary(); ?>
				</div>
			</div>
		</div>
		<div class="footer__lower">
			<div class="footer__logo-wrapper"><img src="<?= get_template_directory_uri() ?>/assets/img/logo-envol-white.svg" /></div>
			<p class="footer__copyright"><?= htmlentities($footerField["copyrghtText"]); ?></p>
			<p class="footer__credits">Design & développement : Creano</p>
		</div>
	</footer>
		<!-- /foot -->
	</div><!-- scroll-container -->
</div><!-- /global-container -->

<div class="wp-foot">
	<?php wp_footer(); ?>
	<?php edit_post_link('Edit page', '<p id="edit-post-link" style="position: fixed; top: 200px; right: 0; padding: 5px 10px; background: #000; color: #fff; z-index: 9999999">', '</p>', '' , 'no-link'); ?>
</div>

<script type="text/javascript">
  // Get template url in js
  var wpTemplateUrl = '<?= get_bloginfo("template_url"); ?>';
  var wpSiteUrl = '<?= get_site_url() ?>';
</script>

<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
window.jQuery || document.write('<script src="<?php echo get_template_directory_uri(); ?>/assets/js/lib/jquery-3.2.1.min.js"><\/script>')
</script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/dist/js/lib.min.js"></script>
<?php /* preprod <script type="module" src="<?php echo get_template_directory_uri(); ?>/assets/js/app.js"></script> */ ?>
<script type="module" src="<?php echo get_template_directory_uri(); ?>/assets/dist/js/site.js"></script>
</body>
</html>
